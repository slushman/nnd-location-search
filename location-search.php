<?php
/*

Plugin Name: Location Search
Author: Red Castle Services and Slushman
Author URI: http://www.redcastleservices.com/
Description: Location Search by radius

*/
	if ( strpos( __FILE__, WP_PLUGIN_DIR ) === 0 ) {

		$locationsearch_file = plugin_basename( __FILE__ );

	} else {

		$locationsearch_plugins = preg_grep( '#/' . basename( __FILE__ ) . '$#', get_option( 'active_plugins', array() ) );

		if ( !empty( $locationsearch_plugins ) ) {

			$locationsearch_file = current( $locationsearch_plugins );

		} else {

			$locationsearch_file = plugin_basename( $plugin ? $plugin : ( $mu_plugin ? $mu_plugin : ( $network_plugin ? $network_plugin : __FILE__ ) ) );

		}

	} // End of plugin directory check

	$locationsearch_dir = dirname( $locationsearch_file );

	define( 'LOCATIONSEARCH_FILE', $locationsearch_file );
	define( 'LOCATIONSEARCH_PATH', WP_PLUGIN_DIR . '/' . $locationsearch_dir );
	define( 'LOCATIONSEARCH_URL', plugins_url() . '/' . $locationsearch_dir );
	
	if ( !defined( 'LOCATIONSEARCH_TABLE' ) ) {
	
		define( 'LOCATIONSEARCH_TABLE', $wpdb->prefix . 'location_search' );
		
	}

	if ( !defined( 'LOCATIONSEARCH_CAT_TABLE' ) ) {
	
		define( 'LOCATIONSEARCH_CAT_TABLE', $wpdb->prefix . 'location_search_cats' );
		
	}
	
	if ( !defined( 'LOCATIONSEARCH_MAPS_JS_API' ) ) {
	
		$scheme = 'http:';
	
		if ( !empty( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] !== 'off' ) {
	
			$scheme = 'https:';
	
		}
	
		define( 'LOCATIONSEARCH_MAPS_WS_API', $scheme . '//maps.googleapis.com/maps/api/' );
		define( 'LOCATIONSEARCH_MAPS_JS_API', LOCATIONSEARCH_MAPS_WS_API . 'js?' );
	
	}

	include_once( 'classes/locationsearch.php' );
	include_once( 'classes/locations.php' );

	if ( class_exists( 'Location_Search' ) && ( !isset( $location_search ) ) ) {
	
		$location_search = $LocationSearch = new Location_Search();
		
	}

	if ( class_exists( 'LS_Locations' ) && ( !isset( $ls_locations ) || !is_object( $ls_locations ) ) ) {

		$ls_locations = new LS_Locations();
		
	}

	if ( is_admin() ) {

		register_activation_hook( LOCATIONSEARCH_FILE, array( 'LS_Admin', 'on_activate' ) );

		include_once( 'classes/admin.php' );
		include_once( 'classes/options.php' );

		if ( class_exists( 'LS_Options' ) && ( !isset( $ls_options ) || !is_object( $ls_options ) ) ) {
		
			$ls_options = new LS_Options();
			
		}

		if ( class_exists( 'LS_Import_Export' ) && ( !isset( $ls_import_export ) || !is_object( $ls_import_export ) ) )

			$ls_import_export = new LS_Import_Export();

	} else {

		include_once( 'classes/xml-search.php' );
		include_once( 'classes/maps.php' );

		if ( class_exists( 'LS_XML_Search' ) && ( !isset( $ls_xml_search ) ) ) {
		
			$ls_xml_search = new LS_XML_Search();
			
		}

		if ( class_exists( 'LS_Location_Shortcodes' ) && !isset( $ls_location_shortcodes ) ) {
		
			$ls_location_shortcodes = new LS_Location_Shortcodes();
			
		}

		if ( class_exists( 'LS_Map_Factory' ) && !isset( $ls_map_factory ) ) {
		
			$ls_map_factory = new LS_Map_Factory();
			
		}

		if ( class_exists( 'LS_Template_Factory' ) && !isset( $ls_template_factory ) ) {
		
			add_action( 'template_redirect', 'ls_init_templating' );
			
		}
	
	} // End of is_admin check

/**
 * Creates a new instance of LS_Template_Factory
 * 
 * @uses	LS_Template_Factory
 */
	function ls_init_templating() {

		$ls_location_master = new LS_Template_Factory();

	} // End of ls_init_templating()

	if ( class_exists( 'LS_Admin' ) && ( !isset( $ls_admin ) || !is_object( $ls_admin ) ) ) {
	
		$ls_admin = new LS_Admin();
		
	}

?>
