<?php
if ( !class_exists( 'LS_Admin' ) ) {

	class LS_Admin{

/**
 * Constructor
 *
 * @uses	add_action
 */		
		function __construct() {
		
			// add_action( 'admin_menu', array( &$this, 'menu_shuffle' ), 20 );
			add_action( 'admin_menu', array( &$this, 'add_menu' ), 20 );
			add_action( 'admin_head', array( &$this, 'load_admin_scripts' ) );
		
		} // End of __construct()
		
/**
 * menu_shuffle function.
 * 
 * @access public
 *
 * @global	$menu
 * @global	$location_search
 * @global	$ls_options
 * @global	$ls_help
 * @global	$ls_import_export
 *
 * @return void
 */
		/*function menu_shuffle() {
		
			global $menu,$location_search,$ls_options,$ls_help,$ls_import_export;

			$options = $location_search->get_options();

			foreach( $menu as $key => $value ) {
		
				if ( in_array( 'edit.php?post_type=ls-location' , $value ) ) {
		
					unset( $menu[$key] );
	
				}
	
			} // End of $menu foreach loop
	
			add_menu_page(__('LocationSearch Options', 'LocationSearch'), 'Location Search', apply_filters( 'ls-admin-permissions-ls-options', 'publish_posts' ), 'locationsearch', array( &$ls_options, 'print_page' ), LOCATIONSEARCH_URL.'/images/icon.png' );

			add_submenu_page( 
				'locationsearch', 
				__('LocationSearch: Options', 'LocationSearch'),
				__( 'Options', 'LocationSearch'), 
				apply_filters( 'ls-admin-permissions-ls-options', 'manage_options' ), 
				'locationsearch', 
				array( &$ls_options, 'print_page' ) 
			);

			add_submenu_page( 'locationsearch', __('LocationSearch: Add Location', 'LocationSearch'), __( 'Add Location', 'LocationSearch' ), apply_filters( 'ls-admin-permissions-ls-add-locations', 'publish_posts' ), 'post-new.php?post_type=ls-location' );
			add_submenu_page( 'locationsearch', __('LocationSearch: Edit Locations', 'LocationSearch'), __( 'Edit Locations', 'LocationSearch' ), apply_filters( 'ls-admin-permissions-ls-edit-locations', 'publish_posts' ), 'edit.php?post_type=ls-location' );

		} // End of menu_shuffle()*/

/**
 * Adds the plugin settings page to the appropriate admin menu
 *
 * @since	0.1
 *
 * @global 	$ls_options
 * 
 * @uses	add_submenu_page
 */				
		function add_menu() {

			global $ls_options;

			add_submenu_page(
				'edit.php?post_type=ls-location',
				__( 'LocationSearch: Options', 'LocationSearch' ),
				__( 'Options', 'LocationSearch'),
				apply_filters( 'ls-admin-permissions-ls-options', 'manage_options' ),
				'locationsearch',
				array( &$ls_options, 'print_page' )
			);

			/*add_submenu_page( 
				'locationsearch', 
				__('LocationSearch: Options', 'LocationSearch'),
				__( 'Options', 'LocationSearch'), 
				apply_filters( 'ls-admin-permissions-ls-options', 'manage_options' ), 
				'locationsearch', 
				array( &$ls_options, 'print_page' ) 
			);*/
			
		} // End of add_menu()		

/**
 * load_admin_scripts function.
 * 
 * @access public
 *
 * @global	$current_screen
 *
 * @return void
 */
		function load_admin_scripts(){
		
			global $current_screen;
			
			if ( 'toplevel_page_locationsearch' == $current_screen->id ) :
		
				?>
				<script type="text/javascript">
				jQuery(document).ready(function($) {
					if ($(document).width() < 1300) {
						$('.postbox-container').css({'width': '99%'});
					}
					else {
						$('.postbox-container').css({'width': '49%'});
					}
					
					if ($('#autoload').val() == 'none') {
						$('#lock_default_location').attr('checked', false);
						$('#lock_default_location').attr('disabled', true);
						$('#lock_default_location_label').addClass('disabled');
					}
					
					$('#autoload').change(function() {
						if ($(this).val() != 'none') {
							$('#lock_default_location').attr('disabled', false);
							$('#lock_default_location_label').removeClass('disabled');
						}
						else {
							$('#lock_default_location').attr('checked', false);
							$('#lock_default_location').attr('disabled', true);
							$('#lock_default_location_label').addClass('disabled');
						}
					});
					
					$('#address_format').siblings().addClass('hidden');
					if ($('#address_format').val() == 'town, province postalcode')
						$('#order_1').removeClass('hidden');
					else if ($('#address_format').val() == 'town province postalcode')
						$('#order_2').removeClass('hidden');
					else if ($('#address_format').val() == 'town-province postalcode')
						$('#order_3').removeClass('hidden');
					else if ($('#address_format').val() == 'postalcode town-province')
						$('#order_4').removeClass('hidden');
					else if ($('#address_format').val() == 'postalcode town, province')
						$('#order_5').removeClass('hidden');
					else if ($('#address_format').val() == 'postalcode town')
						$('#order_6').removeClass('hidden');
					else if ($('#address_format').val() == 'town postalcode')
						$('#order_7').removeClass('hidden');
					
					$('#address_format').change(function() {
						$(this).siblings().addClass('hidden');
						if ($(this).val() == 'town, province postalcode')
							$('#order_1').removeClass('hidden');
						else if ($(this).val() == 'town province postalcode')
							$('#order_2').removeClass('hidden');
						else if ($(this).val() == 'town-province postalcode')
							$('#order_3').removeClass('hidden');
						else if ($(this).val() == 'postalcode town-province')
							$('#order_4').removeClass('hidden');
						else if ($(this).val() == 'postalcode town, province')
							$('#order_5').removeClass('hidden');
						else if ($(this).val() == 'postalcode town')
							$('#order_6').removeClass('hidden');
						else if ($(this).val() == 'town postalcode')
							$('#order_7').removeClass('hidden');
					});
					
				});
				</script>			
				<?php
			endif;

		} // End of load_admin_scripts()

/**
 * on_activate function.
 * 
 * @access public
 * @return void
 */
		function on_activate() {
		
			//$current = get_site_transient( 'update_plugins' );
			//if ( !isset( $current->checked[LOCATIONSEARCH_FILE] ) ) {
			return; // <--- Remove to enable

			$options = get_option( 'LocationSearch_options' );

			if ( empty( $options ) ) {

				$options = array( 'auto_locate' => 'html5' );
				update_option( 'LocationSearch_options', $options );

			}

		} // End of on_activate()

	} // End of LS_Admin

} // End of class check
?>
