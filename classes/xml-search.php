<?php
if ( !class_exists( 'LS_XML_Search' ) ) {

	class LS_XML_Search{

/**
 * Constructor
 * 
 * @uses	add_action
 */
		function __construct() {
		
			add_action( 'template_redirect', array( &$this, 'init_search' ) );

			// Add shortcode(s)
			add_shortcode( 'nearbylocations', array( &$this, 'nearby_locations' ) );

		} // End of __construct()

/**
 * init_search function.
 * 
 * @global	$wpdb
 *
 * @return void
 */
		function init_search() {

			if ( isset( $_GET['ls-xml-search'] ) ) {

				global $wpdb;

				remove_filter( 'the_title', 'at_title_check' );

				$defaults['lat'] 		= $defaults['lng'] = $defaults['radius'] = $defaults['namequery'] = $defaults['address'] = $defaults['city'] = $defaults['state'] = $defaults['zip'] = $defaults['onlyzip'] = $defaults['country'] = $defaults['limit'] = false;
				$defaults['query_type'] = 'distance';
				$defaults['pid'] 		= 0;
				$input 					= array_filter( array_intersect_key( $_GET, $defaults ) ) + $defaults;
				$lstaxes 				= array();
				$locs 					= array( 'address', 'address2', 'city', 'state', 'zip', 'country', 'phone', 'fax', 'email', 'url', 'special' );

				foreach ( $locs as $loc ) {

					$loc_map['location_' . $loc] = $loc;

				} // End of $locs foreach loop

				if ( $taxonomies = get_object_taxonomies( 'ls-location' ) ) {

					foreach ( $taxonomies as $key => $tax ) {

						$phpsafe 		= str_replace( '-', '_', $tax );
						$_GET 			+= array( $phpsafe => '' );
						$lstaxes[$tax] 	= $_GET[$phpsafe];

					} // End of $taxonomies foreach loop

				} // End of $taxonomies check

				// Define my empty strings
				$distance_select = $distance_having = $distance_order = '';

				// We're going to do a hard limit to 5000 for now.
				$limit = ( !$input['limit'] || $input['limit'] > 250 ? "LIMIT 250" : 'LIMIT ' . absint( $input['limit'] ) );				
				$limit = apply_filters( 'ls-xml-search-limit', $limit );

				// Locations within specific distance or just get them all?
				$distance_select = $wpdb->prepare( "( 3959 * ACOS( COS( RADIANS(%s) ) * COS( RADIANS( lat_tbl.meta_value ) ) * COS( RADIANS( lng_tbl.meta_value ) - RADIANS(%s) ) + SIN( RADIANS(%s) ) * SIN( RADIANS( lat_tbl.meta_value ) ) ) ) AS distance", $input['lat'], $input['lng'], $input['lat'] ) . ', ';
				$distance_order = 'distance, ';

				if ( $input['radius'] ) {

					$input['radius'] = ( $input['radius'] < 1 ) ? 1 : $input['radius'];
					$distance_having = $wpdb->prepare( "HAVING distance < %d", $input['radius'] );

				}

				$i = 1;
				$taxonomy_join = '';

				foreach ( array_filter( $lstaxes ) as $taxonomy => $tax_value ) {

					$term_ids = explode( ',', $tax_value );

					if ( $term_ids[0] == 'OR' ) {

						unset( $term_ids[0] );

						if ( empty( $term_ids ) ) { continue; }

						$search_values = array( "IN (" . vsprintf( '%d' . str_repeat( ',%d', count( $term_ids ) - 1 ), $term_ids ) . ")" );

					} else {

						$search_values = array();

						foreach ( $term_ids as $term_id ) {

							$search_values[] = sprintf( '= %d', $term_id );

						} // End of $term_ids foreach loop

					} // End of $term_ids check

					foreach ( $search_values as $search_value ) {

						$taxonomy_join .= "
							INNER JOIN
								$wpdb->term_relationships AS term_rel_$i ON posts.ID = term_rel_$i.object_id
							INNER JOIN
								$wpdb->term_taxonomy AS tax_$i ON
									term_rel_$i.term_taxonomy_id = tax_$i.term_taxonomy_id
									AND tax_$i.taxonomy = '$taxonomy'
									AND tax_$i.term_id $search_value
						";
						$i++;

					} // End of $search_values foreach loop

				} // End of $lstaxes foreach loop

				$sql = "SELECT
						lat_tbl.meta_value AS lat,
						lng_tbl.meta_value AS lng,
						$distance_select
						posts.ID,
						posts.post_content,
						posts.post_title
					FROM
						$wpdb->posts AS posts
					INNER JOIN
						$wpdb->postmeta lat_tbl ON lat_tbl.post_id = posts.ID AND lat_tbl.meta_key = 'location_lat'
					INNER JOIN
						$wpdb->postmeta lng_tbl ON lng_tbl.post_id = posts.ID AND lng_tbl.meta_key = 'location_lng'
						$taxonomy_join
					WHERE
						posts.post_type = 'ls-location'
						AND posts.post_status = 'publish'
					GROUP BY
						posts.ID
						$distance_having
					ORDER BY " . apply_filters( 'ls-location-sort-order', $distance_order . ' posts.post_name ASC', $input ) . " " . $limit;

				$sql = apply_filters( 'ls-xml-search-locations-sql', $sql );

// SLUSHMAN: Add zipsearch to check zip code or city/state metadata before performing radius search

				$locations = $this->db_search_first( $input, $loc_map );

				if ( !isset( $locations ) || !$locations ) {

					$locations = $wpdb->get_results( $sql );

				}

				if ( !$locations ) {
				
					// Print empty XML
					$locations = array();
				
				} else {

					// Start looping through all locations i found in the radius
					foreach ( $locations as $key => $value ) {
				
						// Add postmeta data to location
						$custom_fields = get_post_custom( $value->ID );
				
						foreach ( $loc_map as $key => $field ) {
						
							$value->$field = ( isset( $custom_fields[$key][0] ) ? $custom_fields[$key][0] : '' );
						
						} // End of $loc_map foreach loop

						$value->postid 	= $value->ID;
						$value->name 	= apply_filters( 'the_title', $value->post_title );
						$the_content 	= trim( $value->post_content );
					
						if ( !empty( $the_content ) ) {
					
							$the_content = apply_filters( 'the_content', $the_content );
					
						}
					
						$value->description = $the_content;
						$value->permalink 	= get_permalink( $value->ID );
						$value->permalink 	= apply_filters( 'the_permalink', $value->permalink );
						$value->taxes 		= array();
					
						foreach ( $lstaxes as $taxonomy => $tax_value ) {
					
							$phpsafe_tax 		= str_replace( '-', '_', $taxonomy );
							$local_tax_names 	= '';

							// Get all taxes for this post
							if ( $local_taxes = wp_get_object_terms( $value->ID, $taxonomy, array( 'fields' => 'names' ) ) ) {

								$local_tax_names = implode( ', ', $local_taxes );
					
							}

							$value->taxes[$phpsafe_tax] = $local_tax_names;
					
						} // End of $lstaxes foreach loop
					
					} // End of $locations foreach loop
				
				}

				$locations = apply_filters( 'ls-xml-search-locations', $locations );

				$this->print_json( $locations, $lstaxes );
			
			} // End of $_GET check
		
		} // End of init_search()

/**
 * Prints the JSON output
 * 
 * @access public
 *
 * @param 	mixed 	$dataset
 * @param 	array 	$lstaxes
 *
 * @uses	do_action
 * @uses	json_encode
 *
 * @return void
 */
		function print_json( $dataset, $lstaxes ) {
		
			header( 'Status: 200 OK', false, 200 );
			header( 'Content-type: application/json' );
			do_action( 'ls-xml-search-headers' );

			do_action( 'ls-print-json', $dataset, $lstaxes );

			echo json_encode( $dataset );
			die();
		
		} // End of print_json
	
/**
 * Fetches an array of locations from nnd_db_query, then formats found results
 * to the correct format for the print_json function.
 *
 * @since 0.1
 *
 * @param	string			$input		The form input
 *
 * @uses	nnd_db_query
 * @uses 	get_post_custom
 * @uses 	get_permalink
 * @uses 	apply_filters
 *
 * @return	array | bool	$return		Either an array of location posts objects or FALSE if none are found
 */
		function db_search_first ( $input, $loc_map ) {

			$locations = $this->nnd_db_query( $input, 'strict' );
			
			if ( !$locations ) { 

				$locations = $this->nnd_db_query( $input, 'loose' );

			}

			if ( !$locations ) { return FALSE; }

			$locations = $this->format_db_search( $locations, $loc_map );

			return $locations;

		} // End of db_search_first()

/**
 * Fetches an array of locations based on the zip code, city and/or state. Coverts alphanumeric
 * zip codes to the first three characters before using WP_Query.
 *
 * Performs two possible serach types for city and state: strict, which uses the '=' comparator
 * and loose, which uses the 'LIKE' comparator.
 *
 * @since 0.1
 *
 * @param	string			$input		The form input
 *
 * @uses	WP_Query
 *
 * @return	array | bool	$return		Either an array of location posts objects or FALSE if none are found
 */
		function nnd_db_query( $input, $type = '' ) {

			$args['post_type'] 					= 'ls-location';
			$args['posts_per_page'] 			= '-1';
			$args['post_status'] 				= 'publish';

			if ( isset( $input['city'] ) ) { $input['city'] = ucwords( $input['city'] ); }
			if ( isset( $input['state'] ) ) { $input['state'] = strtoupper( $input['state'] ); }

			if ( $input['zip'] != '' ) {

				$args['meta_key'] 					=
				$args['meta_query'][0]['key']		= 'location_zip';
				$args['meta_query'][0]['value']		= ( is_numeric( $input['zip'] ) ? $input['zip'] : substr( $input['zip'], 0, 3 ) );
				$args['meta_query'][0]['compare']	= 'LIKE';

			} elseif ( $input['city'] != '' && $input['state'] != '' ) {

				$args['meta_key'] 					=
				$args['meta_query'][0]['key']		= 'location_city';
				$args['meta_query'][0]['value']		= $input['city'];
				$args['meta_query'][0]['compare']	= ( $type == 'strict' ? '=' : 'LIKE' );
				$args['meta_query'][1]['key']		= 'location_state';
				$args['meta_query'][1]['value']		= $input['state'];
				$args['meta_query'][1]['compare']	= ( $type == 'strict' ? '=' : 'LIKE' );

			} elseif ( $input['city'] != '' || $input['state'] != '' ) {

				$which 	= ( $input['city'] != '' ? 'city' : 'state' );

				$args['meta_key'] 					=
				$args['meta_query'][0]['key']		= 'location_' . $which;
				$args['meta_query'][0]['value']		= $input[$which];
				$args['meta_query'][0]['compare']	= ( $type == 'strict' ? '=' : 'LIKE' );

			} // End input checks

			$query = new WP_Query( $args );

			if ( $query->post_count < 1 ) { return FALSE; }

			return $query;

		} // End of nnd_db_query()

/**
 * Formats the locations found in db_first_search function
 *
 * @since 0.1
 *
 * @param	array			$locations		The locations found in the radius search
 * @param   array			$loc_map  		The location map array
 *
 * @uses    get_post_custom
 * @uses    get_permalink
 * @uses    apply_filters
 *
 * @return   array			$encode
 */
		function format_db_search( $locations ) {

			$filter = array();
			$i 		= 0;

			foreach ( $locations->posts as $post ) {

				$custom 	= get_post_custom( $post->ID );
				$permalink	= get_permalink( $post->ID );
				$filter[$i] = new StdClass;

				$filter[$i]->lat 			= $custom['location_lat'][0];
				$filter[$i]->lng 			= $custom['location_lng'][0];
				$filter[$i]->distance 		= 0;
				$filter[$i]->ID 			= $filter[$i]->postid = $post->ID;
				$filter[$i]->post_content 	= $post->post_content;
				$filter[$i]->post_title 	= $post->post_title;
				$filter[$i]->name 			= $post->name;
				$filter[$i]->description 	= $post->description;
				$filter[$i]->permalink 		= $permalink;
				$filter[$i]->taxes 			= array();
				$i++;

			}

			return $filter;

		} // End of format_db_search()

/**
 * init_search function.
 * 
 * @global	$wpdb
 *
 * @return void
 */
		function radius_search( $input ) {

			global $wpdb;

			$defaults['lat'] 		= $defaults['lng'] = $defaults['radius'] = $defaults['namequery'] = $defaults['address'] = $defaults['city'] = $defaults['state'] = $defaults['zip'] = $defaults['onlyzip'] = $defaults['country'] = $defaults['limit'] = false;
			$defaults['query_type'] = 'distance';
			$defaults['pid'] 		= 0;
			$input 					= array_filter( array_intersect_key( $input, $defaults ) ) + $defaults;
			$distance_select 		= $distance_having = $distance_order = '';

			// We're going to do a hard limit to 5000 for now.
			$limit = ( !$input['limit'] || $input['limit'] > 250 ? "LIMIT 250" : 'LIMIT ' . absint( $input['limit'] ) );

			// Locations within specific distance or just get them all?
			$distance_select = $wpdb->prepare( "( 3959 * ACOS( COS( RADIANS(%s) ) * COS( RADIANS( lat_tbl.meta_value ) ) * COS( RADIANS( lng_tbl.meta_value ) - RADIANS(%s) ) + SIN( RADIANS(%s) ) * SIN( RADIANS( lat_tbl.meta_value ) ) ) ) AS distance", $input['lat'], $input['lng'], $input['lat'] ) . ', ';
			$distance_order = 'distance, ';

			if ( $input['radius'] ) {

				$input['radius'] = ( $input['radius'] < 1 ) ? 1 : $input['radius'];
				$distance_having = $wpdb->prepare( "HAVING distance < %d", $input['radius'] );

			}

			$sql = "SELECT
					lat_tbl.meta_value AS lat,
					lng_tbl.meta_value AS lng,
					$distance_select
					posts.ID,
					posts.post_content,
					posts.post_title
				FROM
					$wpdb->posts AS posts
				INNER JOIN
					$wpdb->postmeta lat_tbl ON lat_tbl.post_id = posts.ID AND lat_tbl.meta_key = 'location_lat'
				INNER JOIN
					$wpdb->postmeta lng_tbl ON lng_tbl.post_id = posts.ID AND lng_tbl.meta_key = 'location_lng'
				WHERE
					posts.post_type = 'ls-location'
					AND posts.post_status = 'publish'
				GROUP BY
					posts.ID
					$distance_having
				ORDER BY " . apply_filters( 'ls-location-sort-order', $distance_order . ' posts.post_name ASC', $input ) . " " . $limit;

			return $wpdb->get_results( $sql );
		
		} // End of radius_search()

/**
 * search_form function.
 *
 * @param 	string		$mode		The name for the mode hidden field
 *
 * @uses	none_empty
 * @uses	zip_codes_first
 * @uses	get_post_custom
 * @uses	send_wp_mail
 * @uses	get_permalink
 * @uses	input_field
 * @uses	textarea
 * @uses	hidden_field
 * @uses	wp_nonce_field
 * @uses	esc_attr
 */
		function nearby_locations() {
			
			if ( get_post_type( get_the_ID() ) != 'ls-location' ) { return; }

			$custom 			= get_post_custom( get_the_ID() );
			$find['city'] 		= $custom['location_city'][0];
			$find['state'] 		= $custom['location_state'][0];
			$find['lat'] 		= $custom['location_lat'][0];
			$find['lng'] 		= $custom['location_lng'][0];
			$find['radius'] 	= '100';
			$find['limit']		= '4';
			$find['address'] 	= $find['zip'] = '';
			$find['querytype'] 	= 'all';

			$locations = $this->radius_search( $find, 'nearby' );

			if ( $locations ) { 

				echo '<div class="nearby_locations">';
				echo '<p class="nearby_header">Other Nearby Locations:</p>';

				foreach ( $locations as $key => $location ) {

					if ( $location->ID == get_the_ID() ) {

						unset( $locations[$key] );
						$locations = array_values( $locations );

					}

				} // End of $locations foreach loop

				if ( count( $locations ) >= 1 ) {

					foreach ( $locations as $key => $location ) {

						echo '<p class="nearby_location"><a href="' . get_permalink( $location->ID ) . '" class="nearby_link">' . $location->post_title . '</a></p>';

					} // End of $locations foreach loop

				} // End of $locations count

				echo '</div>';

			} // End of $locations check

		} // End of nearby_locations()



/*****************************		End of Functions		*****************************/				
	
	} // End of LS_XML_Search

} // End of class check
