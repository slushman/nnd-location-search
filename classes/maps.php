<?php
/**
 * LS_Map_Factory Class
*/
if ( ! class_exists( 'LS_Map_Factory' ) ) {

	class LS_Map_Factory {

		var $map_atts;
		var $locations;

/**
 * Constructor
 * 
 * @uses	set_map_atts
 * @uses	add_action
 */
		function __construct() {

			$this->set_map_atts();

            if ( isset( $_GET['ls_map_iframe'] ) ) {
        
                add_action( 'template_redirect', array( &$this, 'add_iframe_locations' ), 2 );
                add_action( 'template_redirect', array( &$this, 'generate_iframe' ) );
        
            }
		
		} // End of __construct()

/**
 * set_map_atts function.
 * 
 * @access public
 *
 * @global	$location_search
 *
 * @param 	array 	$atts 	(default: array())
 *
 * @return 	void
 */
		function set_map_atts( $atts=array() ) {

			global $location_search;
           
            $locations = ! empty ( $_GET['location_ids'] ) ? explode( ',', $_GET['location_ids'] ) : array();

            if ( isset( $_GET['ls_map_iframe'] ) ) {
			
                if ( ! empty( $_GET['map_width'] ) )
                    $atts['map_width'] = $_GET['map_width'];
                if ( ! empty( $_GET['map_height'] ) )
                    $atts['map_height'] = $_GET['map_height'];
                if ( ! empty( $_GET['pan_control'] ) )
                    $atts['panControl'] = $_GET['pan_control'];
                if ( ! empty( $_GET['zoom_control'] ) )
                    $atts['zoomControl'] = $_GET['zoom_control'];
                if ( ! empty( $_GET['scale_control'] ) )
                    $atts['scaleControl'] = $_GET['scale_control'];
                if ( ! empty( $_GET['street_view_control'] ) )
                    $atts['streetViewControl'] = $_GET['street_view_control'];
                if ( ! empty( $_GET['map_type_control'] ) )
                    $atts['mapTypeControl'] = $_GET['map_type_control'];
                if ( ! empty( $_GET['map_type'] ) )
                    $atts['mapType'] = $_GET['map_type'];
                if ( empty( $_GET['default_lat'] ) )
                    $atts['default_lat'] = get_post_meta( $locations[0], 'location_lat', true );
                if ( empty( $_GET['default_lng'] ) )
                    $atts['default_lng'] = get_post_meta( $locations[0], 'location_lng', true );
                if ( empty( $_GET['zoom_level'] ) )
                    $atts['zoom_level'] = 15;
           
            }

			$defaults = $location_search->get_options();

			$merged_atts = wp_parse_args( $atts, $defaults );

			$this->map_atts = $merged_atts;	

		} // End of set_map_atts()

/**
 * add_location function.
 * 
 * @access 	public
 *
 * @param 	mixed 	$location
 *
 * @uses	get_metadata
 *
 * @return 	void
 */
		function add_location( $location ) {

            if ( is_object( $location ) )
                $location = $location->ID;

			if ( $location_data = get_metadata( 'post', $location ) ) {
			
				$location_array = array(
					'id' 	=> $location,
					'lat'	=> ! empty( $location_data['location_lat'][0] ) ? $location_data['location_lat'][0] : false,
					'lng'	=> ! empty( $location_data['location_lng'][0] ) ? $location_data['location_lng'][0] : false
				);

				if ( $location_array['lat'] && $location_array['lng'] )
					$this->locations[$location] = $location_array;	
			
			}

		} // End of add_location()

/**
 * add_iframe_locations function.
 * 
 * @access 	public
 *
 * @return 	void
 */
        function add_iframe_locations() {

            $locations = ! empty ( $_GET['location_ids'] ) ? explode( ',', $_GET['location_ids'] ) : array();
            
            foreach( $locations as $location ) {
            
            	$this->add_location( $location );
            
            }

        } // End of add_iframe_locations()

/**
 * get_map function.
 * 
 * @access public
 * @return void
 */
        function get_map() {
        
        	

        } // End of get_map()

/**
 * get_iframe_embed function.
 * 
 * @access public
 *
 * @uses	esc_url
 * @uses	site_url
 * @uses	esc_attr
 *
 * @return	string		$iframe		The HTML code for an iFrame
 */
         function get_iframe_embed() {

            $atts = $this->map_atts;
            $locations = array_keys( $this->locations );

            $iframe = '<iframe width="' . $atts['map_width'] . '" height="' . $atts['map_height'] . '" frameborder=0 scrolling="no" src="' . esc_url( site_url() ) . '?ls_map_iframe=1&map_width=' . esc_attr( $atts['map_width'] ) . '&map_height=' . esc_attr( $atts['map_height'] ) . '&location_ids=' . esc_attr( implode( ',', $locations ) ) . '"></iframe>';

            return $iframe;

        } // End of get_iframe_embed()

/**
 * generate_iframe function.
 * 
 * @access 	public
 *
 * @global	$location_search
 *
 * @uses	set_map_atts
 * @uses	wp_enqueue_script
 * @uses	esc_js
 * @uses	get_metadata
 * @uses	get_the_title
 * @uses	esc_attr
 *
 * @return 	void
 */
        function generate_iframe() {

            if ( ! empty( $_GET['ls_map_iframe'] ) ) {
               
                global $location_search;
               
                $this->set_map_atts();
                $atts = $this->map_atts;

                wp_enqueue_script('jquery');
                ?>
                <html style='margin-top:0 !important;padding-top:0 !important;'>
                    <head>
                        <?php wp_head(); ?>
                        <style type='text/css'>* { margin:0; padding:0; }</style>
                        <script src="<?php echo esc_url( LOCATIONSEARCH_MAPS_JS_API . '?v=3&amp;sensor=false&amp;language=' . $atts['default_language'] . '&amp;region=' . $atts['default_country'] ); ?>" type="text/javascript"></script> 
                    </head>
                    <body>

                        <script type="text/javascript"> 

                        var map = null;
                        var geocoder = null;
                        var latlng = new google.maps.LatLng( '<?php echo esc_js( $atts['default_lat'] ) ;?>', '<?php echo esc_js( $atts['default_lng'] ); ?>' );

                        function initialize() {
                            var myOptions = {
                                zoom: parseInt(<?php echo esc_js( $atts['zoom_level'] ); ?>),
                                center: latlng,
                                panControl: <?php echo ($atts['panControl']) ? 'true' : 'false'; ?>,
                                zoomControl: <?php echo ($atts['zoomControl']) ? 'true' : 'false'; ?>,
                                scaleControl: <?php echo ($atts['scaleControl']) ? 'true' : 'false'; ?>,
                                streetViewControl: <?php echo ($atts['streetViewControl']) ? 'true' : 'false'; ?>,
                                mapTypeControl: <?php echo ($atts['mapTypeControl']) ? 'true' : 'false'; ?>,
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                draggable: false
                            };

                            map = new google.maps.Map( document.getElementById( "map_canvas" ), myOptions );
                            geocoder = new google.maps.Geocoder();
                        }

                        function addMapMarkers() {

                            var markersArray = [];
                            <?php foreach( $this->locations as $location ) { ?>
                                
                                <?php $customvals = get_metadata( 'post', $location['id'] ); ?>
                                
                                var name = '<?php echo esc_js( get_the_title( $location['id'] ) ); ?>';
                                var address = '<?php echo esc_js( $customvals['location_address'][0] ); ?>';
                                var address2 = '<?php echo esc_js( $customvals['location_address2'][0] ); ?>';
                                var city = '<?php echo esc_js( $customvals['location_city'][0] ); ?>';
                                var state = '<?php echo esc_js( $customvals['location_state'][0] ); ?>';
                                var zip = '<?php echo esc_js( $customvals['location_zip'][0] ); ?>';
                                var country = '<?php echo esc_js( $customvals['location_country'][0] ); ?>';
                                var email = '<?php echo esc_js( $customvals['location_email'][0] ); ?>';
                                var url = '<?php echo esc_js( $customvals['location_url'][0] ); ?>';
                                var phone = '<?php echo esc_js( $customvals['location_phone'][0] ); ?>';
                                var fax = '<?php echo esc_js( $customvals['location_fax'][0] ); ?>';
                                var special = '<?php echo esc_js( $customvals['location_special'][0] ); ?>';
                                
                                map.setCenter(latlng, 13);
                                var markerOptions = {};
                                if ( 'function' == typeof window.locationsearchCustomMarkers ) {
                                        markerOptions = locationsearchCustomMarkers( name, address, address2, city, state, zip, country, '', url, phone, fax, email, special, '', '', '');
                                }
                                markerOptions.map = map;
                                
                                markerOptions.position = new google.maps.LatLng( '<?php echo esc_js( $location['lat'] ) ;?>', '<?php echo esc_js( $location['lng'] ); ?>' );
                                var marker = new google.maps.Marker( markerOptions );
                                markersArray.push(marker);
                            <?php } ?>

                        }
                        jQuery(document).ready( function() { initialize(); addMapMarkers(); } );

                        </script> 

                        <div id="map_canvas" style="height: <?php echo esc_attr( $_GET['map_height'] ); ?>; width: <?php echo esc_attr( $_GET['map_width'] ); ?>; border: 1px solid #eee; overflow: hidden"></div> 

                    </body>
                </html>
                <?php
                die();
                
            }

        } // End of generate_iframe()

	} // End of LS_Map_Factory

} // End of class check
?>