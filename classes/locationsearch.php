<?php
if ( !class_exists( 'Location_Search' ) ) {

	class Location_Search {
	
		var $plugin_url;
		var $plugin_domain = 'LocationSearch';
		
/**
 * Constructor
 * 
 * @uses	load_plugin_textdomain
 * @uses	add_shortcode
 * @uses	add_action
 * @uses	add_filter
 */
		function __construct() {
			
			$plugin_dir = basename( LOCATIONSEARCH_PATH );
			load_plugin_textdomain( $this->plugin_domain, 'wp-content/plugins/' . $plugin_dir . '/lang', $plugin_dir . '/lang' );
			
			$this->plugin_url = LOCATIONSEARCH_URL;
						
			// Add shortcode(s)
			add_shortcode( 'locationsearch', array( &$this, 'display_map' ) );
			add_shortcode( 'minisearch', array( &$this, 'mini_search' ) );
			add_shortcode( 'nearbylocations', array( &$this, 'nearby_locations' ) );
						
			// Enqueue frontend scripts & styles into <head>
			add_action( 'template_redirect', array( &$this, 'enqueue_frontend_scripts_styles' ) );
			
			// Enqueue backend scripts
			add_action( 'init', array( &$this, 'enqueue_backend_scripts_styles' ) );

			// Add hook for master js file
			add_action( 'template_redirect', array( &$this, 'google_map_js_script' ) );

			// Add hook for general options js file
			add_action( 'init', array( &$this, 'general_options_js_script' ) );
			
			// Query vars
			add_filter( 'query_vars', array( &$this, 'register_query_vars' ) );

			// Backwards compat for core ls taxonomies
			add_filter( 'ls_category-text', array( &$this, 'backwards_compat_categories_text' ) );
			add_filter( 'ls_tag-text', array( &$this, 'backwards_compat_tags_text' ) );
			add_filter( 'ls_day-text', array( &$this, 'backwards_compat_days_text' ) );
			add_filter( 'ls_time-text', array( &$this, 'backwards_compat_times_text' ) );

		} // End of __construct()

/**
 * Generates the code to display the map
 * 
 * @access	public
 *
 * @param	array	$atts
 * 
 * @uses	get_options
 * @uses	parse_shortcode_atts
 * @uses	location_search_form
 * @uses	apply_filters
 * @uses	esc_js
 *
 * @return 	mixed	The map
 */
		function display_map( $atts ) {

			$options 	= $this->get_options();
			$atts 		= $this->parse_shortcode_atts( $atts );

			extract( $atts );

			$to_display = '';
			$to_display .= $this->location_search_form( $atts );


			// Hide map?
			$hidemap = ( $hide_map ) ? "display:none; " : '';

			// Hide list?
			$hidelist = $hide_list ? "display:none; " : '';
			
			// Map Width and height
			$map_width 	= ( '' == $map_width ) ? $options['map_width'] : $map_width;
			$map_height = ( '' == $map_height ) ? $options['map_height'] : $map_height;

			// Updating Div
			$ls_updating_div_size = apply_filters( 'ls_updating_img_size', 'height:' . $map_height . ';width:' . $map_width . ';' );
			$to_display .= '<div id="locationsearch-updating" style="'. $ls_updating_div_size. '"><img src="' . $ls_updating_img_src . '" alt="' . __( 'Loading new locations', 'LocationSearch' ) . '" /></div>';
			$to_display .= '<div id="locationsearch" style="' . $hidemap . 'width: ' . $map_width . '; height: ' . $map_height . ';"></div>';
			$to_display .= '<div id="results" style="' . $hidelist . 'width: ' . $map_width . ';"></div>';
			$to_display .= '<script type="text/javascript">';
			$to_display .= '(function($) { ';

			// Load Locations
 			$is_ls_search 		= isset( $_REQUEST['location_is_search_results'] ) ? 1 : 0;
			$do_search_function = '
				load_locationsearch( lat, lng, aspid, ascid, asma, shortcode_zoom_level, map_type, shortcode_autoload );
				//searchLocations( ' . absint( $is_ls_search ) . ' );
			';

			$to_display .= '$(document).ready(function() {
				var lat = "' . esc_js( $default_lat ) . '";
				var lng = "' . esc_js( $default_lng ) . '";
				var aspid = "' . esc_js( $adsense_publisher_id ) . '";
				var ascid = "' . esc_js( $adsense_channel_id ) . '";
				var asma = "' . esc_js( $adsense_max_ads ) . '";
				var shortcode_zoom_level = "' . esc_js( $zoom_level ) . '";
				var map_type = "' . esc_js( $map_type ) . '";
				var shortcode_autoload = "' . esc_js( $autoload ) . '";
				var auto_locate = "' . esc_js( $options['auto_locate'] ) . '";
				var ls_autolocate_complete = false;
				geocoder = new google.maps.Geocoder();

				if ( !' . absint( $is_ls_search ) . ' && auto_locate == "ip" ) {
					jQuery.getJSON( "http://freegeoip.net/json/?callback=?", function(location) {
						lat = location.latitude;
						lng = location.longitude;

                    	if ( document.getElementById("location_search_city_field") ) {
							document.getElementById("location_search_city_field").value = location.city;
						}
	                    if ( document.getElementById("location_search_country_field") ) {
							document.getElementById("location_search_country_field").value = location.country_code;
						}
        	            if ( document.getElementById("location_search_state_field") ) {
							document.getElementById("location_search_state_field").value = location.region_code;
						}
                	    if ( document.getElementById("location_search_zip_field") ) {
							document.getElementById("location_search_zip_field").value = location.zipcode;
						}
						if ( document.getElementById("location_search_default_lat" ) ) {
							document.getElementById("location_search_default_lat").value = lat;
						}
						if ( document.getElementById("location_search_default_lng" ) ) {
							document.getElementById("location_search_default_lng").value = lng;
						}
						' . $do_search_function . '
						searchLocations( 1 );
					}).error(function() {
					 	' . $do_search_function . '
						searchLocations( ' . absint( $is_ls_search ) . ' );
					});
				}
				else if ( !' . absint( $is_ls_search ) . ' && auto_locate == "html5" ) {
					// Ugly hack for FireFox "Not Now" option
					setTimeout(function () { 
						if ( ls_autolocate_complete == false ) {
							' . $do_search_function . ' searchLocations( 0 );
						}
					}, 10000);

					navigator.geolocation.getCurrentPosition(
						function(position) {
							lat = position.coords.latitude;
							lng = position.coords.longitude;
							if ( document.getElementById("location_search_default_lat" ) ) {
								document.getElementById("location_search_default_lat").value = lat;
							}
							if ( document.getElementById("location_search_default_lng" ) ) {
								document.getElementById("location_search_default_lng").value = lng;
							}
							ls_autolocate_complete = true;
							' . $do_search_function . '
							searchLocations( 1 );
						},
						function(error) {
							ls_autolocate_complete = true;
							' . $do_search_function . '
							searchLocations( ' . absint( $is_ls_search ) . ' );
						},
						{
							maximumAge:300000,
							timeout:5000
						}
					);
				}
				else {
					ls_autolocate_complete = true;
					' . $do_search_function . '
					searchLocations( ' . absint( $is_ls_search ) . ' );
				}
			';

			$to_display .= '});';
			$to_display .= '})(jQuery);';
			$to_display .= '</script>';
			$to_display .= '';

			return apply_filters( 'ls-display-map', $to_display, $atts );
			
		} // End of display_map()

/**
 * Returns the location search form
 * 
 * @access public
 *
 * @global	$post
 *
 * @param	array	$atts
 * 
 * @uses	get_options
 * @uses	parse_shortcode_atts
 * @uses	get_object_taxonomies
 * @uses	get_form_field_names_from_shortcode_atts
 * @uses	apply_filters
 * @uses	get_query_var
 * @uses	esc_attr
 * @uses	add_distance_field
 * @uses	show_taxonomy_filter
 * @uses	
 *
 * @return void
 */
		function location_search_form( $atts ) {
		
			global $post;

			// Grab default LocationSearch options
			$options = $this->get_options();

			// Merge default locationsearch options with default shortcode options and provided shortcode options
			$atts = $this->parse_shortcode_atts( $atts );

			// Create individual vars for each att
			extract( $atts );

			// Array of the names for all taxonomies registered with ls-location post type
			$ls_tax_names = get_object_taxonomies( 'ls-location' );

			// Array of field names for this form (with label syntax stripped
			$form_field_names = $this->get_form_field_names_from_shortcode_atts( $search_fields );

			// Form onsubmit, action, and method values
			$on_submit 	= apply_filters( 'ls-location-search-onsubmit', ' onsubmit="searchLocations( 1 ); return false; "', $post->ID );
			$action 	= apply_filters( 'ls-location-search-action', get_permalink(), $post->ID );
			$method 	= apply_filters( 'ls-location-search-method', 'post', $post->ID );			

			// Form Field Values
			$address_value		= get_query_var( 'location_search_address' );
			$city_value 		= isset( $_REQUEST['location_search_city'] ) ? $_REQUEST['location_search_city'] : '';
			$state_value 		= isset( $_REQUEST['location_search_state'] ) ? $_REQUEST['location_search_state'] : '';
			$zip_value 			= get_query_var( 'location_search_zip' );
			$country_value 		= get_query_var( 'location_search_country' );
			$radius_value	 	= isset( $_REQUEST['location_search_distance'] ) ? $_REQUEST['location_search_distance'] : $radius;
			$limit_value		= isset( $_REQUEST['location_search_limit'] ) ? $_REQUEST['location_search_limit'] : $limit;
 			$is_ls_search		= isset( $_REQUEST['location_is_search_results'] ) ? 1 : 0;

			// Normal Field inputs
			$ffi['zip']			= array( 'label' => apply_filters( 'ls-search-label-zip', __( 'Zip: ', 'LocationSearch' ), $post ), 'input' => '<input type="text" id="location_search_zip_field" name="location_search_zip" value="' . esc_attr( $zip_value ) . '" />' );
			$ffi['city']		= array( 'label' => apply_filters( 'ls-search-label-city', __( 'City: ', 'LocationSearch' ), $post ), 'input' => '<input type="text"  id="location_search_city_field" name="location_search_city" value="' . esc_attr( $city_value ) . '" />' );
			$ffi['state']		= array( 'label' => apply_filters( 'ls-search-label-state', __( 'State: ', 'LocationSearch' ), $post ), 'input' => '<input type="text" id="location_search_state_field" name="location_search_state" value="' . esc_attr( $state_value ) . '" />' );
			$ffi['submit']		= array( 'label' => '', 'input' => '<input type="submit" value="' . apply_filters( 'ls-search-label-search', __('Search', 'LocationSearch'), $post ) . '" id="location_search_submit_field" class="submit" />' );
			$ffi['distance']	= $this->add_distance_field( $radius_value, $units );
			$hidden_fields 		= array();

			// Visible Taxonomy Field Inputs
			foreach ( $ls_tax_names as $tax_name ) {
		
				if ( in_array( $tax_name, $form_field_names ) && $this->show_taxonomy_filter( $atts, $tax_name ) )
					$ffi[$tax_name] = $this->add_taxonomy_fields( $atts, $tax_name );
				else
					$hidden_fields[] = '<input type="hidden" name="location_search_' . str_replace( '-', '_', $tax_name ) . '_field" value="1" />';
		
			} // End of $ls_tax_names foreach loop

			// More Taxonomy Fields
			foreach ( $ls_tax_names as $tax_name ) {
		
				$hidden_fields[] = '<input type="hidden" id="avail_' . str_replace( '-', '_', $tax_name ) . '" value="' . $atts[str_replace( '-', '_', $tax_name )] . '" />';
		
			}

			// Hide search?
			$hidesearch 		= $hide_search ? " style='display:none;' " : '';
			$location_search  	= '<div id="map_search" >';
			$location_search 	.= '<a id="map_top"></a>';
			$location_search 	.= '<form ' . $on_submit . ' name="location_search_form" id="location_search_form" action="' . $action . '" method="' . $method . '">';
			$location_search 	.= '<table class="location_search"' . $hidesearch . '>';
			$location_search 	.= apply_filters( 'ls-location-search-table-top', '', $post );
			$location_search 	.= '<tr><td colspan="' . $search_form_cols . '" class="location_search_title">' . apply_filters( 'ls-location-search-title', $search_title, $post->ID ) . '</td></tr>';

			// Loop through field inputs and print table
			$search_form_tr 	= 0;
			$search_form_trs 	= array();
			$search_field_td 	= 1;
			$search_fields 		= explode( '||', $search_fields);

			if ( !in_array( 'submit', $search_fields ) ) {
		
				$hidden_fields[] = '<input type="submit" style="display: none;" />';
		
			}

			foreach ( $search_fields as $field_key => $field_labelvalue ) {
		
				switch( substr( $field_labelvalue, 0, 8 ) ) {
		
					case 'labelbr_' :
						$field_label	= true;
						$field_br 		= '<br />';
						$field_value 	= substr( $field_labelvalue, 8 );
						break;

					case 'labelsp_' :
						$field_label	= true;
						$field_br		= '&nbsp';
						$field_value	= substr( $field_labelvalue, 8 );
						break;

					case 'labeltd_' :
						$field_label	= true;
						$field_br		= "</td>\n\t\t<td>";
						$field_value	= substr( $field_labelvalue, 8 );
						break;

					default :
						$field_label	= false;
						$field_br		= '';
						$field_value	= $field_labelvalue;
		
				} // End of switch

				// Back compat for class names
				switch ( $field_value ) {

					case 'ls-category' :
						$class_value = 'cat';
						break;
					case 'ls-tag' :
						$class_value = 'tag';
						break;
					case 'ls-day' :
						$class_value = 'day';
						break;
					case 'ls-time' :
						$class_value = 'time';
						break;
					case 'address' :
						$class_value = 'street';
						break;
					default :
						$class_value = $field_value;

				} // End of switch

				// Print open TR if on column 1
				if ( 1 === $search_field_td ) {

					$search_form_tr_data = "\n\t<div id='location_search_" . esc_attr( $search_form_tr ) . "_tr' class='location_search_row'>";
					$search_form_tr++;

				}

				// Print field for this position
				if ( 'span' == $field_value ) {

					if ( $tr_data_array = explode( '', $search_form_tr_data ) ) {
						$target_td = end( $tr_data_array );

						end( $tr_data_array );
						$key = key( $tr_data_array );

						if ( 'colspan="' != substr( $target_td, 0, 9 ) ) {
					
							$tr_data_array[$key] = 'colspan="2" ' . $target_td;
							//echo "<pre>";print_r($tr_data_array);die();
					
						} else {
					
							$numcells = substr( $target_td, 9, 1);
							$tr_data_array[$key] = substr_replace( $target_td, $numcells + 1, 9, 1 );
					
						}

						$search_form_tr_data = implode( ' ', $tr_data_array );
					
					}
				
				} else {
					// The extra column needs to be counted independent of whether the field_value isset so that we don't lose count.
					if ( "" == $field_br ) {
				
						$search_field_td++;
						$field_br = "</td>\n\t\t<td id='location_search_" . esc_attr( substr( $field_labelvalue, 8 ) ) . "_fields'>";
				
					}

					if ( isset( $ffi[$field_value] ) && 'empty' != $field_value && 'span' != $field_value ) {
				
						// If the field_br is a space, we need to wrap the label in a div so that it floats left too.
						if ( '&nbsp' == $field_br ) {
				
							$ffi[$field_value]['label'] = '<div class="float_text_left">' . $ffi[$field_value]['label'] . '</div>';
				
						}

						$taxonomy_class = ( isset( $options['taxonomies'][$field_value] ) ? 'location_search_taxonomy_cell' : '' );
						$search_form_tr_data .= "\n\t\t<td class='location_search_" . esc_attr( $class_value ) . "_cell $taxonomy_class location_search_cell'>";

						if ( $field_label ) {
				
							if ( isset( $ffi[$field_value]['label'] ) ) {
				
								$search_form_tr_data .= $ffi[$field_value]['label'];
				
							}

							$search_form_tr_data .= $field_br;
				
						}

						$search_form_tr_data .= isset( $ffi[$field_value]['input'] ) ? $ffi[$field_value]['input'] . '</td>' : '</td>';
					} else {
						$search_form_tr_data .= "\n\t\t<td class='location_search_empty_cell location_search_cell'></td>";
				
					}
				
				}

				// Print close TR if on column 3 or higher (for safety)
				if ( $search_form_cols <= $search_field_td ) {
				
					$search_form_tr_data .= "\n\t</div>";
					$search_field_td = 0;

					// Only keep the rows that contain an actionable item.
					if ( strpos( $search_form_tr_data, 'input' ) || strpos( $search_form_tr_data, 'select' ) ) {
				
						$search_form_trs[] = $search_form_tr_data;
				
					}
				
				}

				// Bump search field count
				$search_field_td++;
			
			}

			// Add table fields
			if ( !empty( $search_form_trs ) ) {
			
				$location_search .= implode( ' ', $search_form_trs );
			
			}

			$location_search .= apply_filters( 'ls-location-search-before-submit', '', $post );
			$location_search .= '</table>';

			// Add hidden fields
			if ( !empty( $hidden_fields ) ) {
			
				$location_search .= implode( ' ', $hidden_fields );
			
			}

			// Lat / Lng
			$location_search .= "<input type='hidden' id='location_search_default_lat' value='" . $default_lat . "' />";
			$location_search .= "<input type='hidden' id='location_search_default_lng' value='" . $default_lng . "' />";

			// Hidden value for limit
			$location_search .= "<input type='hidden' id='location_search_limit' value='" . $limit_value . "' />";

			// Hidden value set to true if we got here via search
			$location_search .= "<input type='hidden' id='location_is_search_results' name='ls-location-search' value='" . $is_ls_search . "' />";
			$location_search .= '</form>';
			$location_search .= '</div>'; // close map_search div

			return apply_filters( 'ls_location_search_form', $location_search, $atts );
		
		} // End of location_search_form()

/**
 * Separates form field names from label syntax attached to them when submitted via shortcode
 * 
 * @access	public
 *
 * @param	mixed	$fields
 *
 * @return	array	$field_names	An array of all the field names from the shortcode attributes
 */
		function get_form_field_names_from_shortcode_atts( $fields ) {

			// String to array
			$fields = explode( '||', $fields);

			foreach( $fields as $key => $field ) {

				switch( substr( $field, 0, 8 ) ) {

					case 'labelbr_' :
						$field_names[] 	= substr( $field, 8 );
						break;
					case 'labelsp_' :
						$field_names[]	= substr( $field, 8 );
						break;
					case 'labeltd_' :
						$field_names[]	= substr( $field, 8 );
						break;
					default :
						$field_names[]	= $field;
				
				} // End of switch

			} // End of $fields foreach loop

			return (array) $field_names;

		} // End of get_form_field_names_from_shortcode_atts()

/**
 * Determines if we're supposed to show this taxonomy's filter options in the form
 * 
 * @access public
 *
 * @param	array	$atts
 * @param	string	$tax_name
 *
 * @return	bool	TRUE if $show_taxes_filter is TRUE and FALSE otherwise
 */
		function show_taxonomy_filter( $atts, $tax_name ) {

			// Convert tax_name to PHP safe equiv
			$php_tax_name = str_replace( '-', '_', $tax_name );

			// Convert Given Taxonomy's 'show filter' into a generic one
			$key 				= 'show_' . $php_tax_name . '_filter';
			$show_taxes_filter 	= $atts[$key];

			if ( false != $show_taxes_filter && 'false' != $show_taxes_filter ) { return true; }

			return false;

		} // End of show_taxonomy_filter()

/**
 * Adds Distance field to form
 * 
 * @access public
 *
 * @param	int		$radius_value
 * @param	string	$units
 *
 * @uses	get_search_radii
 * @uses	selected
 *
 * @return	array	The label and distance input for the distance field
 */
		function add_distance_field( $radius_value, $units ) {
			
			global $post;

			// Distance
			$distance_input	= '<select id="location_search_distance_field" name="location_search_distance" >';
			
			foreach ( $this->get_search_radii() as $value ) {
			
				$r 				= (int) $value;
				$distance_input .= '<option value="' . $value . '"' . selected( $radius_value, $value, false ) . '>' . $value . ' ' . $units . "</option>\n";
			
			}
			
			$distance_input .= '</select>';

			return array( 'label' => apply_filters( 'ls-search-label-distance', __( 'Select a radius: ', 'LocationSearch' ), $post ), 'input' => $distance_input );
		
		} // End of add_distance_field()

/**
 * Adds taxonomy fields to search form
 * 
 * @access public
 *
 * @param mixed $atts
 * @param mixed $taxonomy
 *
 * @uses	get_taxonomy
 * @uses	get_options
 * @uses	parse_shortcode_atts
 * @uses	get_terms
 * @uses	apply_filters
 * @uses	get_term_by
 * @uses	esc_attr
 *
 * @return void
 */
		function add_taxonomy_fields( $atts, $taxonomy ) {
			
			global $post;

			// Get taxonomy object or return empty;
			if ( !$tax_object = get_taxonomy( $taxonomy ) ) { return ''; }

			$options 	= $this->get_options();
			$atts 		= $this->parse_shortcode_atts( $atts );

			extract( $atts );

			$php_taxonomy = str_replace( '-', '_', $taxonomy );

			// Convert Specific Taxonomy var names and var values to Generic var names and var values
			$taxonomies 		= $atts[$php_taxonomy];
			$tax_hidden_name	= 'avail_' . $php_taxonomy;	
			$show_taxes_filter	= $atts['show_' . $php_taxonomy . '_filter'];
			$tax_field_name		= $php_taxonomy;

			// This originates at the comma separated list of taxonomy ids in the shortcode. ie: ls_category='1,3,5'
			$taxes_avail = $atts[$tax_hidden_name];

			// Place available taxes in array
			$taxes_avail = explode( ',', $taxes_avail );
			$taxes_array = array();

			// Loop through all cats and create array of available cats
			if ( $all_taxes = get_terms( $taxonomy ) ) {

				foreach ( $all_taxes as $key => $value ){
			
					if ( '' == $taxes_avail[0] || in_array( $value->term_id, $taxes_avail ) ) {
			
						$taxes_array[] = $value->term_id;
			
					}
			
				} // End of $all_taxes foreach loop

			}

			$taxes_avail = $taxes_array;

			// Show taxes filters if allowed
			$tax_search = '';
			$tax_label  = apply_filters( $php_taxonomy . '-text', __( $tax_object->labels->singular_name . ': ' ), 'LocationSearch' );

			$taxes_array = apply_filters( 'ls-search-from-taxonomies', $taxes_array, $taxonomy );

			if ( 'checkboxes' == $taxonomy_field_type ) {
		
				// Print checkbox for each available cat
				foreach( $taxes_array as $key => $taxid ) {
		
					if( $term = get_term_by( 'id', $taxid, $taxonomy ) ) {
		
						$tax_checked = isset( $_REQUEST['location_search_' . $tax_field_name . '_' . esc_attr( $term->term_id ) . 'field'] ) ? ' checked="checked" ' : '';
						$tax_search .= '<label for="location_search_' . $tax_field_name . '_field_' . esc_attr( $term->term_id ) . '" class="no-linebreak"><input rel="location_search_' . $tax_field_name . '_field" type="checkbox" name="location_search_' . $tax_field_name . '_' . esc_attr( $term->term_id ) . 'field" id="location_search_' . $tax_field_name . '_field_' . esc_attr( $term->term_id ) . '" value="' . esc_attr( $term->term_id ) . '" ' . $tax_checked . '/> ' . esc_attr( $term->name ) . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label> ';
		
					}
		
				}
		
			} elseif( 'select' == $taxonomy_field_type ) {
		
				// Print selectbox if that's what we're doing
				$tax_select  = "<select id='location_search_" . esc_attr( $tax_field_name ) . "_select' name='location_search_" . esc_attr( $tax_field_name ) . "_select' >";
				$tax_select .= "<option value=''>" . apply_filters( 'ls-search-tax-select-default', __( 'Select a value', 'LocationSearch' ), $taxonomy ) . "</option>";
		
				foreach( $taxes_array as $key => $taxid ) {
		
					if( $term = get_term_by( 'id', $taxid, $taxonomy ) ) {
		
						$tax_checked = isset( $_REQUEST['location_search_' . esc_attr( $tax_field_name ) . '_select' ] ) ? ' selected="selected" ' : '';
						$tax_select .= '<option rel="location_search_' . esc_attr( $tax_field_name ) . '_select_val"' . ' value="' . esc_attr( $term->term_id ) . '" ' . $tax_checked . '>' . esc_attr( $term->name ) . '</option>';
		
					}
		
				} // End of $taxes_array foreach loop
		
				$tax_select .= "</select>";

				if ( !empty( $taxid ) )
					$tax_search .= $tax_select;
		
			}

			return array( 'label' => $tax_label, 'input' => $tax_search );
		
		} // End of add_taxonomy_fields()

/**
 * Enqueues all the javascript and stylesheets for the front-facing pages
 * 
 * @access public
 *
 * @global	$post
 *
 * @uses	get_options
 * @uses	get_option
 * @uses	flush_rules
 * @uses	delete_option
 * @uses	is_admin
 * @uses	apply_filters
 * @uses	plugins_url
 * @uses	wp_enqueue_style
 * @uses	wp_enqueue_script
 * @uses	get_home_url
 */
		function enqueue_frontend_scripts_styles() {
			
			global $post;
			
			$options = $this->get_options();

			// Register Font Awesome stylesheet
			wp_register_style( 'font-awesome',  plugins_url( 'font-awesome.min.css', dirname( __FILE__ ) ) );

			// Rewrite rules if we've changed the location permalink slug
			if ( get_option( 'ls-rewrite-rules' ) ) {
			
				global $wp_rewrite;
			
				$wp_rewrite->flush_rules();
			
				delete_option( 'ls-rewrite-rules' );
			
			}

			// Frontend only
			if ( !is_admin() && is_object( $post ) || apply_filters( 'ls-force-frontend-js', '__return_false' ) ) {
			
				// Bail if we're not showing on all pages and this isn't a map page
				if ( !in_array( $post->ID, explode( ',', $options['map_pages'] ) ) && !in_array( 0, explode( ',', $options['map_pages'] ) ) )
					return false;

				// Check for use of custom stylesheet and load styles
				if ( strstr( $options['map_stylesheet'], 'locationsearch-styles' ) )
					$style_url = plugins_url() . '/' . $options['map_stylesheet'];
				else
					$style_url = LOCATIONSEARCH_URL . '/' . $options['map_stylesheet'];

				// Load styles
				wp_enqueue_style( 'locationsearch-map-style', $style_url );

				// Load styles
				wp_enqueue_style( 'font-awesome' );

				// Scripts
				wp_enqueue_script( 'locationsearch-master-js', get_home_url() . '?locationsearch-master-js=1&smpid=' . $post->ID, array( 'jquery' ) );

				// Google API v3 does not need a key
				$url_params = array(
					'sensor' => 'false',
					'v' => '3',
					'language' => $options['default_language'],
					'region' => $options['default_country'],
				);
			
				if ( $options['adsense_for_maps'] ) {
			
					$url_params['libraries'] = 'adsense';
			
				}
			
				wp_enqueue_script( 'locationsearch-google-api', LOCATIONSEARCH_MAPS_JS_API . http_build_query( $url_params, '', '&amp;' ) );


			
			}
		
		} // End of enqueue_frontend_scripts_styles()

/**
 * Enqueues all the javascript and stylesheets for the admin
 * 
 * @access public
 *
 * @uses	is_admin
 * @uses	wp_enqueue_style
 * @uses	wp_enqueue_script
 * @uses	get_home_url
 */
		function enqueue_backend_scripts_styles() {
		
			// Admin only
			if ( is_admin() ) {
		
				$options = $this->get_options();
		
				wp_enqueue_style( 'locationsearch-admin', LOCATIONSEARCH_URL . '/style.css' );

				// LocationSearch General options
				if ( isset( $_GET['page'] ) && 'locationsearch' == $_GET['page'] )
					wp_enqueue_script( 'locationsearch-general-options-js', get_home_url() . '/?locationsearch-general-options-js', array( 'jquery' ) );

				// Google API v3 does not need a key
				$url_params = array(
					'v' => '3',
					'sensor' => 'false',
					'language' => $options['default_language'],
					'region' => $options['default_country'],
				);
		
				wp_enqueue_script( 'locationsearch-google-api', LOCATIONSEARCH_MAPS_JS_API . http_build_query( $url_params, '', '&amp;' ) );
		
			}
		
		} // End of enqueue_backend_scripts_styles()

/**
 * JS Script for general options page
 * 
 * @access public
 *
 * @uses	do_action
 * @uses	get_options
 *
 * @return void
 */
		function general_options_js_script() {
		
			if ( !isset( $_GET['locationsearch-general-options-js'] ) )
				return;

			header( 'Status: 200 OK', false, 200 );
			header( 'Content-type: application/x-javascript' );
			do_action( 'ls-master-js-headers' );

			$options = $this->get_options();

			do_action( 'ls-general-options-js' );
			?>
			function codeAddress() {
				// if this is modified, modify mirror function in master-js php function
				var d_address = document.getElementById("default_address").value;

				geocoder = new google.maps.Geocoder();
				geocoder.geocode( { 'address': d_address }, function( results, status ) {
					if ( status == google.maps.GeocoderStatus.OK ) {
						var latlng = results[0].geometry.location;
						document.getElementById("default_lat").value = latlng.lat();
						document.getElementById("default_lng").value = latlng.lng();
					} else {
						alert("Geocode was not successful for the following reason: " + status);
					}
				});
			}
			<?php
			die();
		
		} // End of general_options_js_script()

/**
 * Prints the JS
 * 
 * @access public
 *
 * @uses	get_options
 * @uses	esc_js
 * @uses	apply_filters
 * @uses	get_ls_taxonomies
 * @uses	get_home_url
 * @uses	do_action
 * @uses	get_object_taxonomies
 *
 * @return void
 */
		function google_map_js_script() {
		
			if ( !isset( $_GET['locationsearch-master-js'] ) )
				return;

			header( "HTTP/1.1 200 OK" );
			header( "Content-type: application/x-javascript" );
			$options = $this->get_options();

			?>
			var default_lat 			= <?php echo esc_js( $options['default_lat'] ); ?>;
			var default_lng 			= <?php echo esc_js( $options['default_lng'] ); ?>;
			var default_radius 			= <?php echo esc_js( $options['default_radius'] ); ?>;
			var zoom_level 				= '<?php echo esc_js( $options['zoom_level'] ); ?>';
			var map_width 				= '<?php echo esc_js( $options['map_width'] ); ?>';
			var map_height 				= '<?php echo esc_js( $options['map_height'] ); ?>';
			var special_text 			= '<?php echo esc_js( $options['special_text'] ); ?>';
			var units 					= '<?php echo esc_js( $options['units'] ); ?>';
			var limit 					= '<?php echo esc_js( $options['results_limit'] ); ?>';
			var plugin_url 				= '<?php echo esc_js( LOCATIONSEARCH_URL ); ?>';
			var visit_website_text 		= '<?php echo apply_filters( 'ls-visit-website-text', __( 'Visit Website', 'LocationSearch' ) ); ?>';
			var get_directions_text		= '<?php echo apply_filters( 'ls-get-directions-text', __( 'Get Directions', 'LocationSearch' ) ); ?>';
			var location_tab_text		= '<?php echo apply_filters( 'ls-location-text', __( 'Location', 'LocationSearch' ) ); ?>';
			var description_tab_text	= '<?php echo apply_filters( 'ls-description-text', __( 'Description', 'LocationSearch' ) ); ?>';
			var phone_text				= '<?php echo apply_filters( 'ls-phone-text', __( 'Phone', 'LocationSearch' ) ); ?>';
			var fax_text				= '<?php echo apply_filters( 'ls-fax-text', __( 'Fax', 'LocationSearch' ) ); ?>';
			var email_text				= '<?php echo apply_filters( 'ls-email-text', __( 'Email', 'LocationSearch' ) ); ?>';

			var taxonomy_text = {};
			<?php
			if ( $taxonomies = $this->get_ls_taxonomies( 'array', '', true, 'object' ) ) {
		
				foreach( $taxonomies as $taxonomy ) {
		
					?>
					taxonomy_text.<?php echo $taxonomy->name; ?> = '<?php echo apply_filters( $taxonomy->name . '-text', __( $taxonomy->labels->name, 'LocationSearch' ) ); ?>';
					<?php
		
				}
		
			}
		
			?>
			var noresults_text			= '<?php echo apply_filters( 'ls-no-results-found-text', __( 'No results found.', 'LocationSearch' ) ); ?>';
			var default_domain 			= '<?php echo esc_js( $options['default_domain'] ); ?>';
			var address_format 			= '<?php echo esc_js( $options['address_format'] ); ?>';
			var siteurl					= '<?php echo esc_js( get_home_url() ); ?>';
			var map;
			var geocoder;
			var autoload				= '<?php echo esc_js( $options['autoload'] ); ?>';
			var auto_locate				= '<?php echo esc_js( $options['auto_locate'] ); ?>';
			var markersArray = [];
			var infowindowsArray = [];

			function clearInfoWindows() {
				if (infowindowsArray) {
					for (var i=0;i<infowindowsArray.length;i++) {
						infowindowsArray[i].close();
					}
				}
			}

			function clearOverlays() {
				if (markersArray) {
					for (var i=0;i<markersArray.length;i++) {
						markersArray[i].setMap(null);
					}
				}
			}

			//function load_locationsearch( lat, lng, aspid, ascid, asma ) {
			function load_locationsearch( lat, lng, aspid, ascid, asma, shortcode_zoom_level, map_type, shortcode_autoload ) {

				zoom_level = shortcode_zoom_level;
                autoload = shortcode_autoload;
				<?php 
				/*
				if ( '' == $options['api_key'] ) {
					?>
					jQuery( "#locationsearch" ).html( "<p style='padding:10px;'><?php printf( __( 'You must enter an API Key in <a href=\"%s\">General Settings</a> before your maps will work.', 'LocationSearch' ), admin_url( 'admin.php?page=locationsearch' ) ); ?></p>" );
				<?php
				}
				*/

				do_action( 'ls-load-locationsearch-js-top' );
				?>
  
				if ( lat == 0 ) {
					lat = '<?php echo esc_js( $options['default_lat'] ); ?>';
				}

				if ( lng == 0 ) {
					lng = '<?php echo esc_js( $options['default_lng'] ); ?>';
				}

				var latlng = new google.maps.LatLng( lat, lng );
				var myOptions = {
					zoom: parseInt(zoom_level),
					center: latlng,
					disableDefaultUI: true,
					mapTypeId: google.maps.MapTypeId[map_type] 
				};
				map = new google.maps.Map( document.getElementById( "locationsearch" ), myOptions );

				// Adsense for Google Maps
				<?php 
				if ( '' != $options['adsense_pub_id'] && $options['adsense_for_maps'] ) {

					$default_adsense_publisher_id = isset( $options['adsense_pub_id'] ) ? $options['adsense_pub_id'] : '';
					$default_adsense_channel_id = isset( $options['adsense_channel_id'] ) ? $options['adsense_channel_id'] : '';
					$default_adsense_max_ads = isset( $options['adsense_max_ads'] ) ? $options['adsense_max_ads'] : 2;

					?>

					// Adsense ID. If no shortcode, check for options. If not options, use blank string.
					if ( aspid == 0 )
						aspid = '<?php echo esc_js( $default_adsense_publisher_id ); ?>';

					// Channel ID. If no shortcode, check for options. If not options, use blank string.
					if ( ascid == 0 )
						ascid = '<?php echo esc_js( $default_adsense_channel_id ); ?>';

					// Max ads per map. If no shortcode, check for options. If no options, use 2.
					if ( asma == 0 )
						asma = '<?php echo esc_js( $default_adsense_max_ads ); ?>';

					var publisher_id = aspid;

					var adUnitDiv = document.createElement('div');
					var adUnitOptions = {
						channelNumber: ascid,
						format: google.maps.adsense.AdFormat.HALF_BANNER,
						position: google.maps.ControlPosition.TOP,
						map: map,
						visible: true,
						publisherId: publisher_id
					};
					adUnit = new google.maps.adsense.AdUnit(adUnitDiv, adUnitOptions);
				<?php
				}
				
				do_action( 'ls-load-locationsearch-js-bottom' );				
				?>
			}

			function codeAddress() {
				// if this is modified, modify mirror function in general-options-js php function 
				var d_address = document.getElementById("default_address").value;

				geocoder = new google.maps.Geocoder();
				geocoder.geocode( { 'address': d_address }, function( results, status ) {
					if ( status == google.maps.GeocoderStatus.OK ) {
						var latlng = results[0].geometry.location;
						document.getElementById("default_lat").value = latlng.lat();
						document.getElementById("default_lng").value = latlng.lng();
					} else {
						alert("Geocode was not successful for the following reason: " + status);
					}
				});
			}

			function codeNewAddress() {
				if (document.getElementById("store_lat").value != '' && document.getElementById("store_lng").value != '') {
					document.new_location_form.submit();
				}
				else {
					var address = '';
					var street = document.getElementById("store_address").value;
					var city = document.getElementById("store_city").value;
					var state = document.getElementById("store_state").value;
					var country = document.getElementById("store_country").value;

					if (street) { address += street + ', '; }
					if (city) { address += city + ', '; }
					if (state) { address += state + ', '; }
					address += country;

					geocoder = new google.maps.Geocoder();
					geocoder.geocode( { 'address': address }, function( results, status ) {
						if ( status == google.maps.GeocoderStatus.OK ) {
							var latlng = results[0].geometry.location;
							document.getElementById("store_lat").value = latlng.lat();
							document.getElementById("store_lng").value = latlng.lng();
							document.new_location_form.submit();
						} else {
							alert("Geocode was not successful for the following reason: " + status);
						}
					});
				}
			}

			function codeChangedAddress() {
				var address = '';
				var street = document.getElementById("store_address").value;
				var city = document.getElementById("store_city").value;
				var state = document.getElementById("store_state").value;
				var country = document.getElementById("store_country").value;

				if (street) { address += street + ', '; }
				if (city) { address += city + ', '; }
				if (state) { address += state + ', '; }
				address += country;

				geocoder = new google.maps.Geocoder();
				geocoder.geocode( { 'address': address }, function( results, status ) {
					if ( status == google.maps.GeocoderStatus.OK ) {
						var latlng = results[0].geometry.location;
						document.getElementById("store_lat").value = latlng.lat();
						document.getElementById("store_lng").value = latlng.lng();
					} else {
						alert("Geocode was not successful for the following reason: " + status);
					}
				});
			}

			function searchLocations( is_search ) {
				// Init searchData
				var searchData = {};
				searchData.taxes = {};

				// Set defaults for search form fields
				searchData.address	= '';
				searchData.city 	= '';
				searchData.state	= '';
				searchData.zip		= '';
				searchData.country	= '';

				if ( null != document.getElementById('location_search_address_field') ) {
					searchData.address = document.getElementById('location_search_address_field').value;
				}

				if ( null != document.getElementById('location_search_city_field') ) {
					searchData.city = document.getElementById('location_search_city_field').value;
				}

				if ( null != document.getElementById('location_search_country_field') ) {
					searchData.country = document.getElementById('location_search_country_field').value;;
				}

				if ( null != document.getElementById('location_search_state_field') ) {
					searchData.state = document.getElementById('location_search_state_field').value;
				}

				if ( null != document.getElementById('location_search_zip_field') ) {
					searchData.zip = document.getElementById('location_search_zip_field').value;
				}

				if ( null != document.getElementById('location_search_distance_field') ) {
					searchData.radius = document.getElementById('location_search_distance_field').value;
				}

				searchData.lat			= document.getElementById('location_search_default_lat').value;
				searchData.lng			= document.getElementById('location_search_default_lng').value;
				searchData.limit		= document.getElementById('location_search_limit').value; 
				searchData.searching	= document.getElementById('location_is_search_results').value;

				// Do LocationSearch Taxonomies
				<?php 
				if ( $taxnames = get_object_taxonomies( 'ls-location' ) ) {

					foreach ( $taxnames as $name ) {
		
						$php_name = str_replace( '-', '_', $name );
						?>

						// Do taxnonomy for checkboxes
						searchData.taxes.<?php echo $php_name; ?> = '';
						var checks_found = false;
						jQuery( 'input[rel=location_search_<?php echo $php_name; ?>_field]' ).each( function() {
							checks_found = true;
							if ( jQuery( this ).attr( 'checked' ) && jQuery( this ).attr( 'value' ) != null ) {
								<?php echo 'searchData.taxes.' . $php_name; ?> += jQuery( this ).attr( 'value' ) + ',';
							}
						});

						// Do taxnonomy for select box if checks weren't found
						if ( false == checks_found ) {	
							jQuery( 'option[rel=location_search_<?php echo $php_name; ?>_select_val]' ).each( function() {
								if ( jQuery( this ).attr( 'selected' ) && jQuery( this ).attr( 'value' ) != null ) {
									<?php echo 'searchData.taxes.' . $php_name; ?> += jQuery( this ).attr( 'value' ) + ',';
								}
							});
						}

						<?php
		
					} // End of $taxnames foreach loop
		
				} ?>

				var query = '';
				var start = 0;
 
				if ( searchData.address && searchData.address != '' ) {
					query += searchData.address + ', ';
				}

				if ( searchData.city && searchData.city != '' ) {
					query += searchData.city + ', ';
				}

				if ( searchData.state && searchData.state != '' ) {
					query += searchData.state + ', ';
				}

				if ( searchData.zip && searchData.zip != '' ) {
					query += searchData.zip + ', ';
				}

				if ( searchData.country && searchData.country != '' ) {
					query += searchData.country + ', ';
				}

				// Query
				if ( query != null ) {
					query = query.slice(0, -2);
				}

				if ( searchData.limit == '' || searchData.limit == null ) {
					searchData.limit = 0;
				}

				if ( searchData.radius == '' || searchData.radius == null ) {
					searchData.radius = 0;
				}

				// Taxonomies
				<?php 
				if ( $taxnames = get_object_taxonomies( 'ls-location' ) ) {

					foreach ( $taxnames as $name ) {
					
						$php_name = str_replace( '-', '_', $name );
						?>

						if ( <?php echo 'searchData.taxes.' . $php_name; ?> != null ) {
							var _<?php echo $php_name; ?> = <?php echo 'searchData.taxes.' . $php_name; ?>.slice(0, -1);
						} else {
							var _<?php echo $php_name; ?> = '';
						}

						// Append available taxes logic if no taxes are selected but limited taxes were passed through shortcode as available
						if ( '' != document.getElementById('avail_<?php echo $php_name; ?>').value && '' == _<?php echo $php_name; ?> ) {
							_<?php echo $php_name; ?> = 'OR,' + document.getElementById('avail_<?php echo $php_name; ?>').value;
						}

						searchData.taxes.<?php echo $php_name; ?> = _<?php echo $php_name; ?>;

						<?php
				
					} // End of $taxnames foreach loop
				
				}
				?>

				// Load default location if query is empty
				if ( query == '' || query == null ) {

					if ( searchData.lat != 0 && searchData.lng != 0 )
						query = searchData.lat + ', ' + searchData.lng;
					else
						query = '<?php echo esc_js( $options['default_lat'] ); ?>, <?php echo esc_js( $options['default_lng'] ); ?>';

				}

				// Searching
				if ( 1 == searchData.searching || 1 == is_search ) {
					is_search = 1;
					searchData.source = 'search';
				} else {
					is_search = 0;
					searchData.source = 'initial_load';
				}

				geocoder.geocode( { 'address': query }, function( results, status ) {
					if ( status == google.maps.GeocoderStatus.OK ) {
						searchData.center = results[0].geometry.location;
						if ( 'none' != autoload || is_search ) {
						
							if (!searchData.center) {
								searchData.center = new GLatLng( 44.9799654, -93.2638361 );
							}
							searchData.query_type = 'all';
							searchData.mapLock = 'unlock';
							searchData.homeAddress = query;

/* SLUSHMAN: added hiding of the results div if there was no search */
					
							if ( is_search == 0 ) {
								jQuery( "#results" ).css( "display", "none" );
							} else {
								jQuery( "#results" ).css( "display", "inherit" );
								searchLocationsNear(searchData); 
							}
						}
					}
				});
			}

			function searchLocationsNear(searchData) {
				// Radius
				if ( searchData.radius != null && searchData.radius != '' ) {
					searchData.radius = parseInt( searchData.radius );

					if ( units == 'km' ) {
						searchData.radius = parseInt( searchData.radius ) / 1.609344;
					}
				} else if ( autoload == 'all' ) {
					searchData.radius = 0;
				} else {
					if ( units == 'mi' ) {
						searchData.radius = parseInt( default_radius );
					} else if ( units == 'km' ) {
						searchData.radius = parseInt( default_radius ) / 1.609344;
					}
				}

				// Build search URL
				<?php 
				if ( $taxonomies = $this->get_ls_taxonomies( 'array', '', true ) ) {
				
					$js_tax_string = '';
				
					foreach( $taxonomies as $taxonomy ) {
				
						$js_tax_string .= "'&$taxonomy=' + searchData.taxes.$taxonomy + ";
				
					} // End of $taxonomies foreach loop
				
				}
				?>

				var searchUrl = siteurl + '/?ls-xml-search=1&lat=' + searchData.center.lat() + '&lng=' + searchData.center.lng() + '&radius=' + searchData.radius + '&namequery=' + searchData.homeAddress + '&query_type=' + searchData.query_type  + '&limit=' + searchData.limit + <?php echo $js_tax_string; ?>'&address=' + searchData.address + '&city=' + searchData.city + '&state=' + searchData.state + '&zip=' + searchData.zip + '&pid=<?php echo esc_js( absint( $_GET['smpid'] ) ); ?>';

				<?php if ( apply_filters( 'ls-use-updating-image', true ) ) : ?>
				// Display Updating Message and hide search results
				if ( jQuery( "#locationsearch" ).is(":visible") ) {
					jQuery( "#locationsearch" ).hide();
					jQuery( "#locationsearch-updating" ).show();
				}
				<?php endif; ?>
				
				jQuery( "#results" ).html( '' );
				jQuery.get( searchUrl, {}, function(data) {
					<?php if ( apply_filters( 'ls-use-updating-image', true ) ) : ?>
					// Hide Updating Message
					if ( jQuery( "#locationsearch-updating" ).is(":visible") ) {
						jQuery( "#locationsearch-updating" ).hide();
						jQuery( "#locationsearch" ).show();
					}
					<?php endif; ?>

					clearOverlays();

					var results 		= document.getElementById('results');
					results.innerHTML 	= '';
					var markers 		= jQuery( eval( data ) );

					if (markers.length == 0) {
						results.innerHTML = '<h3>' + noresults_text + '</h3>';
						map.setCenter( searchData.center );
						return;
					}

/* SLUSHMAN: 5/11/2013 - Adding redirection to the location's permalink if only one location is found */

					if (markers.length == 1) {

						window.location.href = markers[0].permalink;

					}

					var bounds = new google.maps.LatLngBounds();
					markers.each( function () {
						var locationData 			= this;
						locationData.distance 		= parseFloat(locationData.distance);
						locationData.point 			= new google.maps.LatLng(parseFloat(locationData.lat), parseFloat(locationData.lng));
						locationData.homeAddress 	= searchData.homeAddress;

						var marker 					= createMarker(locationData);
						var sidebarEntry 			= createSidebarEntry(marker, locationData, searchData);

						results.appendChild(sidebarEntry);
						bounds.extend(locationData.point);
					});

					// If the search button was clicked, limit to a 15px zoom
					if ( 'search' == searchData.source ) {
						map.fitBounds( bounds );
						if ( map.getZoom() > 15 ) {
							map.setZoom( 15 );
						}
					} else {
						// If initial load of map, zoom to default settings
						map.setZoom(parseInt(zoom_level));
					}

				});
			}

			function stringFilter(s) {
				filteredValues = "emnpxt%";     // Characters stripped out
				var i;
				var returnString = "";
				for (i = 0; i < s.length; i++) {  // Search through string and append to unfiltered values to returnString.
					var c = s.charAt(i);
					if (filteredValues.indexOf(c) == -1) returnString += c;
				}
				return returnString;
			}

/*
 * Creates the marker for a location
 */
			function createMarker( locationData ) {

				// Init tax heights
				locationData.taxonomyheights = [];

				// Allow plugin users to define Maker Options (including custom images)
				var markerOptions = {};
				if ( 'function' == typeof window.locationsearchCustomMarkers ) {
					markerOptions = locationsearchCustomMarkers( locationData );
				}

				// Allow developers to turn of description in bubble. (Return true to hide)
				<?php if ( true === apply_filters( 'ls-hide-bubble-description', false ) ) : ?>
				locationData.description = '';
				<?php endif; ?>
				
				markerOptions.icon = new google.maps.MarkerImage(
				  '<?php echo esc_js( apply_filters( 'ls-search-marker-image-url', LOCATIONSEARCH_URL . "/images/heartmarker.png" ) ); ?>',
				  new google.maps.Size(20,34),
				  new google.maps.Point(0,0),
				  new google.maps.Point(0,34)
				);
				
				markerOptions.shadow = new google.maps.MarkerImage(
				  '<?php echo esc_js( apply_filters( 'ls-search-marker-image-url', LOCATIONSEARCH_URL . "/images/heartshadow.png" ) ); ?>',
				  new google.maps.Size(40,34),
				  new google.maps.Point(0,0),
				  new google.maps.Point(0,34)
				);
				
				markerOptions.shape = {
				  coord: [16,3,17,4,17,5,17,6,17,7,17,8,17,9,17,10,17,11,16,12,16,13,17,14,16,15,15,16,14,17,14,18,14,19,11,20,11,21,17,22,17,23,16,24,14,25,13,26,12,27,9,27,9,26,9,25,9,24,9,23,9,22,9,21,9,20,9,19,9,18,9,17,7,16,6,15,5,14,4,13,3,12,2,11,2,10,2,9,2,8,2,7,2,6,2,5,2,4,3,3,16,3],
				  type: 'poly'
				};
				
				markerOptions.map = map;
				markerOptions.position = locationData.point;
				var marker = new google.maps.Marker( markerOptions );
				marker.title = locationData.name;
				markersArray.push(marker);

				var mapwidth = Number(stringFilter(map_width));
				var mapheight = Number(stringFilter(map_height));

				var maxbubblewidth = Math.round(mapwidth / 1.5);
				// var maxbubbleheight = Math.round(mapheight / 2.2);
				
				var maxbubbleheight = Math.round(mapheight / 2);

				var fontsize = 12;
				var lineheight = 12;

				if (locationData.taxes.ls_category && locationData.taxes.ls_category != '' ) {
					var titleheight = 3 + Math.floor((locationData.name.length + locationData.taxes.ls_category.length) * fontsize / (maxbubblewidth * 1.5));
				} else {
					var titleheight = 3 + Math.floor((locationData.name.length) * fontsize / (maxbubblewidth * 1.5));
				}

				var addressheight = 2;
				if (locationData.address2 != '') {
					addressheight += 1;
				}
				if (locationData.phone != '' || locationData.fax != '') {
					addressheight += 1;
					if (locationData.phone != '') {
						addressheight += 1;
					}
					if (locationData.fax != '') {
						addressheight += 1;
					}
				}

				for (jstax in locationData.taxes) {
					if ( locationData.taxes[jstax] !== '' ) {
						locationData.taxonomyheights[jstax] = 3 + Math.floor((locationData.taxes[jstax][length]) * fontsize / (maxbubblewidth * 1.5));
					}
				}
				var linksheight = 2;

				var totalheight = titleheight + addressheight;
				for (jstax in locationData.taxes) {
					if ( 'ls_category' != jstax ) {
						totalheight += locationData.taxonomyheights[jstax];
					}
				}
				totalheight = (totalheight + 1) * fontsize;

				if (totalheight > maxbubbleheight) {
					totalheight = maxbubbleheight;
				}

				var html = '<div class="markertext" style="overflow-y: auto; overflow-x: hidden;">';
				html += '<h3 style="margin: 0; padding-top: 0; border-top: none;">';

				if ( '' != locationData.permalink ) {
					html += '<a href="' + locationData.permalink + '">';
				}
				html += locationData.name;

				if ( '' != locationData.permalink ) {
					html += '</a>';
				}

				if (locationData.taxes.ls_category && locationData.taxes.ls_category != null && locationData.taxes.ls_category != '' ) {
					html += '<br /><span class="bubble_category">' + locationData.taxes.ls_category + '</span>';
				}

				html += '</h3>';

				// Phone and Fax Data
				if (locationData.phone != null && locationData.phone != '') {
					html += '<p class="bubble_contact"><span class="bubble_phone">' + phone_text + ': ' + locationData.phone + '</span>';
					if (locationData.email != null && locationData.email != '') {
						html += '<br />' + email_text + ': <a class="bubble_email" href="mailto:' + locationData.email + '">' + locationData.email + '</a>';
					}					
				}

				html += '	</div>';

				google.maps.event.addListener(marker, 'click', function() {
					clearInfoWindows();
					var infowindow = new google.maps.InfoWindow({
						maxWidth: maxbubblewidth,
						content: html
					});
					infowindow.open(map, marker);
					infowindowsArray.push(infowindow);
					window.location = '#map_top';
				});

				return marker;
			}

/*
 * Creates a sidebar entry from the search results
 *
 * marker used for the click trigger at the bottom
 *
 * locationData data used:
 * 	 postid
 *   permalink
 *   name
 *
 * searchData used for directions link   
 */
			function createSidebarEntry(marker, locationData, searchData) {
				var div = document.createElement('div');

				// Beginning of result
				var html = '<div id="location_' + locationData.postid + '" class="result">';

				// Name & distance
				html += '<div class="result_name">';
				
				html += '<a href="' + locationData.permalink + '" class="result_name_title"><h3>';
				html += locationData.name;
				html += '</h3></a></div>';
				html += '</div>';
				html += '<div style="clear: both;"></div>';

				// End of result
				html += '</div><!-- end -->';

				div.innerHTML = html;
				div.style.cursor = 'pointer'; 
				div.style.margin = 0;
				google.maps.event.addDomListener(div, 'click', function() {
					google.maps.event.trigger(marker, 'click');
				});
				return div;
			}
			<?php
			die();
			
		} // End of google_map_js_script()

/**
 * Geocodes a location
 * 
 * @access public
 *
 * @param string $address (default: '')
 * @param string $city (default: '')
 * @param string $state (default: '')
 * @param string $zip (default: '')
 * @param string $country (default: '')
 * @param string $key (default: '')
 *
 * @uses	get_options
 * @uses	wp_remote_get
 * @uses	is_wp_error
 * @uses	json_decode
 *
 * @return	array | bool	An array of data if it exists, otherwise FALSE
 */
		function geocode_location( $address = '', $city = '', $state = '', $zip = '', $country = '', $key = '' ) {
		
			$options 	= $this->get_options();
			// Create URL encoded comma separated list of address elements that != ''
			$to_geocode = urlencode( implode( ', ', array_filter( compact( 'address', 'city', 'state', 'zip', 'country' ) ) ) );

			// Base URL
			$base_url = LOCATIONSEARCH_MAPS_WS_API . 'geocode/json?sensor=false&region=' . substr( $options['default_domain'], strrpos( $options['default_domain'], '.' ) + 1 );

			// Add query
			$request_url 	= $base_url . "&address=" . $to_geocode;
			$response 		= wp_remote_get( $request_url );

			// TODO: Handle this situation better

			if ( is_wp_error( $response ) ) { return FALSE; }

			$body 	= json_decode( $response['body'] );
			$status = $body->status;

			if ( $status == 'OK' ) {
	
				// Successful geocode
				//echo "<pre>";print_r( $body );die();
				$location = $body->results[0]->geometry->location;

				// Format: Longitude, Latitude, Altitude
				$lat = $location->lat;
				$lng = $location->lng;
		
			}

			return compact( 'body', 'status', 'lat', 'lng' );
			
		} // End of geocode_location()

/**
 * Returns list of LocationSearch Taxonomies
 * 
 * @access 	public
 *
 * @param 	string 	$format 	(default: 'array')
 * @param 	string 	$prefix 	(default: '')
 * @param 	bool 	$php_safe 	(default: false)
 * @param 	string 	$output 	(default: 'names')
 *
 * @uses	get_object_taxonomies
 *
 * @return	array	$taxes	An array of the taxonomies
 */
		function get_ls_taxonomies( $format='array', $prefix='', $php_safe=false, $output='names' ) {

			$taxes = array();

			if ( $taxes = get_object_taxonomies( 'ls-location', $output ) ) {

				foreach( $taxes as $key => $tax ) {

					// Convert to PHP safe and add prefix
					if ( $php_safe && 'names' == $output )
						$taxes[$key] = str_replace( '-', '_', $prefix.$tax );
					elseif ( $php_safe )
						$taxes[$key]->name = str_replace( '-', '_', $prefix.$tax->name );

				} // End of $taxes foreach loop

			}

			// Convert to string if needed
			if ( 'string' == $format )
				$taxes = implode( ', ', $taxes );

			return $taxes;

		} // End of get_ls_taxonomies()

/**
 * get_taxonomy_settings function.
 * 
 * @access 	public
 *
 * @param 	mixed 	$taxonomy 	(default: null)
 *
 * @return 	array	
 */
		function get_taxonomy_settings( $taxonomy = null ) {

			$standard_taxonomies['ls-category'] = array( 'singular' => 'Category', 'plural' => 'Categories', 'hierarchical' => true, 'field' => 'category' );
			$standard_taxonomies['ls-tag'] = array( 'singular' => 'Tag', 'plural' => 'Tags', 'field' => 'tags' );
			$standard_taxonomies['ls-day'] = array( 'singular' => 'Day', 'plural' => 'Days', 'field' => 'days', 'description' => 'day of week' );
			$standard_taxonomies['ls-time'] = array( 'singular' => 'Time', 'plural' => 'Times', 'field' => 'times', 'description' => 'time of day' );
		
			if ( empty( $taxonomy ) ) { return $standard_taxonomies; }

			if ( isset( $standard_taxonomies[$taxonomy] ) ) {
			
				return $standard_taxonomies[$taxonomy];
			
			} else {
				
				$singular = ucwords( substr( $taxonomy, strpos( $taxonomy, '-' ) + 1 ) );
				
				return array( 'singular' => $singular, 'plural' => $singular . 's' );
			
			}
		
		} // End of get_taxonomy_settings()

/**
 * Returns the default LocationSearch options	
 * 
 * @access 	public
 *
 * @uses	get_option
 * @uses	get_taxonomy_settings
 * @uses	update_option
 *
 * @return 	array	$options
 */	
		function get_options() {
		
			$options 	= array();
			$saved 		= get_option( 'LocationSearch_options' );

			if ( !empty( $saved ) ) {
		
				$options = $saved;
		
			}

			static $default = null;
			
			if ( empty( $default ) ) {
		
				$default = array(
					'map_width' => '100%',
					'map_height' => '350px',
					'default_lat' => '44.968684',
					'default_lng' => '-93.215561',
					'zoom_level' => '10',
					'default_radius' => '10',
					'map_type' => 'ROADMAP',
					'special_text' => '',
					'default_state' => '',
					'default_country' => 'US',
					'default_language' => 'en',
					'default_domain' => '.com',
					'map_stylesheet' => '/style.css',
					'units' => 'mi',
					'autoload' => 'all',
					'lock_default_location' => false,
					'results_limit' => '20',
					'address_format' => 'town, province postalcode',
					'powered_by' => 0,
					'enable_permalinks' => 1,
					'permalink_slug' => 'location',
					'display_search' => 'show',
					'map_pages' => '0',
					'adsense_for_maps' => 0,
					'adsense_pub_id' => '',
					'adsense_channel_id' => '',
					'adsense_max_ads' => 2,
					//'api_key' => '',
					'auto_locate' => '',
 					'taxonomies' => array(
						'ls-category' => $this->get_taxonomy_settings( 'ls-category' ),
						'ls-tag' => $this->get_taxonomy_settings( 'ls-tag' ),
					),
				);

				$valid_map_type_map = array(
					'ROADMAP' => 'ROADMAP',
					'SATELLITE' => 'SATELLITE',
					'HYBRID' => 'HYBRID',
					'TERRAIN' => 'TERRAIN',
					'G_NORMAL_MAP' => 'ROADMAP',
					'G_SATELLITE_MAP' => 'SATELLITE',
					'G_HYBRID_MAP' => 'HYBRID',
					'G_PHYSICAL_MAP' => 'TERRAIN',
				);

				$options['map_type'] = ( empty( $valid_map_type_map[$options['map_type']] ) ? $default['map_type'] : $valid_map_type_map[$options['map_type']] );
			
			}

			$options += $default;

			if ( isset( $options['days_taxonomy'] ) ) {
			
				if ( !empty( $options['days_taxonomy'] ) ) {
			
					$options['taxonomies']['ls-day'] = $this->get_taxonomy_settings( 'ls-day' );
			
				}
			
				unset( $options['days_taxonomy'] );
			
			}

			if ( isset( $options['time_taxonomy'] ) ) {
			
				if ( !empty( $options['time_taxonomy'] ) ) {
			
					$options['taxonomies']['ls-time'] = $this->get_taxonomy_settings( 'ls-time' );
			
				}
			
				unset( $options['time_taxonomy'] );
			
			}

			if ( $saved != $options ) {
			
				update_option( 'LocationSearch_options', $options );
			
			}
			
			return $options;
		
		} // End of get_options()
		
/**
 * Returns an array of Google domain options
 * 
 * @access public
 *
 * @uses	apply_filters
 *
 * @return	array	$domains_list	An array of domains
 */
		function get_domain_options() {
		
			$domains_list = array(
				'United States' => '.com',
				'Austria' => '.at',
				'Australia' => '.com.au',
				'Bosnia and Herzegovina' => '.com.ba',
				'Belgium' => '.be',
				'Brazil' => '.com.br',
				'Canada' => '.ca',
				'Switzerland' => '.ch',
				'Czech Republic' => '.cz',
				'Germany' => '.de',
				'Denmark' => '.dk',
				'Spain' => '.es',
				'Finland' => '.fi',
				'France' => '.fr',
				'Italy' => '.it',
				'Japan' => '.jp',
				'Netherlands' => '.nl',
				'Norway' => '.no',
				'New Zealand' => '.co.nz',
				'Poland' => '.pl',
				'Russia' => '.ru',
				'Sweden' => '.se',
				'Taiwan' => '.tw',
				'United Kingdom' => '.co.uk',
				'South Africa' => '.co.za'
			);
			
			return apply_filters( 'ls-domain-list', $domains_list );
		
		} // End of get_domain_options()

/**
 * Return an array of regions
 *
 * Region list from http://code.google.com/apis/adwords/docs/appendix/provincecodes.html
 * Used for Maps v3 localization: http://code.google.com/apis/maps/documentation/javascript/basics.html#Localization
 * 
 * @access 	public
 *
 * @uses	apply_filters
 *
 * @return	array	$region_list	An array of regions
 */
		function get_region_options() {
			
			$region_list = array(
				'US' => 'United States',
				'AR' => 'Argentina',
				'AU' => 'Australia',
				'AT' => 'Austria',
				'BE' => 'Belgium',
				'BR' => 'Brazil',
				'CA' => 'Canada',
				'CL' => 'Chile',
				'CN' => 'China',
				'CO' => 'Colombia',
				'HR' => 'Croatia',
				'CZ' => 'Czech Republic',
				'DK' => 'Denmark',
				'EG' => 'Egypt',
				'FI' => 'Finland',
				'FR' => 'France',
				'DE' => 'Germany',
				'HU' => 'Hungary',
				'IN' => 'India',
				'IE' => 'Ireland',
				'IL' => 'Israel',
				'IT' => 'Italy',
				'JP' => 'Japan',
				'MY' => 'Malaysia',
				'MX' => 'Mexico',
				'MA' => 'Morocco',
				'NL' => 'Netherlands',
				'NZ' => 'New Zealand',
				'NG' => 'Nigeria',
				'NO' => 'Norway',
				'PL' => 'Poland',
				'PT' => 'Portugal',
				'RU' => 'Russian Federation',
				'SA' => 'Saudi Arabia',
				'ZA' => 'South Africa',
				'KR' => 'South Korea',
				'ES' => 'Spain',
				'SE' => 'Sweden',
				'CH' => 'Switzerland',
				'TH' => 'Thailand',
				'TR' => 'Turkey',
				'UA' => 'Ukraine',
				'GB' => 'United Kingdom',
			);

			return apply_filters( 'ls-region-list', $region_list );
		
		} // End of get_region_options()

/**
 * Returns an array of countries
 * 
 * @access public
 *
 * @uses	apply_filters
 *
 * @return	array	$country_list	An array of countries
 */
		function get_country_options() {
		
			$country_list = array(
				'US' => 'United States',
				'AF' => 'Afghanistan',
				'AL' => 'Albania',
				'DZ' => 'Algeria',
				'AS' => 'American Samoa',
				'AD' => 'Andorra',
				'AO' => 'Angola',
				'AI' => 'Anguilla',
				'AQ' => 'Antarctica',
				'AG' => 'Antigua and Barbuda',
				'AR' => 'Argentina',
				'AM' => 'Armenia',
				'AW' => 'Aruba',
				'AU' => 'Australia',
				'AT' => 'Austria',
				'AZ' => 'Azerbaijan',
				'BS' => 'Bahamas',
				'BH' => 'Bahrain',
				'BD' => 'Bangladesh',
				'BB' => 'Barbados',
				'BY' => 'Belarus',
				'BE' => 'Belgium',
				'BZ' => 'Belize',
				'BJ' => 'Benin',
				'BM' => 'Bermuda',
				'BT' => 'Bhutan',
				'BO' => 'Bolivia',
				'BA' => 'Bosnia and Herzegowina',
				'BW' => 'Botswana',
				'BV' => 'Bouvet Island',
				'BR' => 'Brazil',
				'IO' => 'British Indian Ocean Territory',
				'BN' => 'Brunei Darussalam',
				'BG' => 'Bulgaria',
				'BF' => 'Burkina Faso',
				'BI' => 'Burundi',
				'KH' => 'Cambodia',
				'CM' => 'Cameroon',
				'CA' => 'Canada',
				'CV' => 'Cape Verde',
				'KY' => 'Cayman Islands',
				'CF' => 'Central African Republic',
				'TD' => 'Chad',
				'CL' => 'Chile',
				'CN' => 'China',
				'CX' => 'Christmas Island',
				'CC' => 'Cocos (Keeling) Islands',
				'CO' => 'Colombia',
				'KM' => 'Comoros',
				'CG' => 'Congo',
				'CD' => 'Congo, The Democratic Republic of the',
				'CK' => 'Cook Islands',
				'CR' => 'Costa Rica',
				'CI' => 'Cote D\'Ivoire',
				'HR' => 'Croatia (Local Name: Hrvatska)',
				'CU' => 'Cuba',
				'CY' => 'Cyprus',
				'CZ' => 'Czech Republic',
				'DK' => 'Denmark',
				'DJ' => 'Djibouti',
				'DM' => 'Dominica',
				'DO' => 'Dominican Republic',
				'TP' => 'East Timor',
				'EC' => 'Ecuador',
				'EG' => 'Egypt',
				'SV' => 'El Salvador',
				'GQ' => 'Equatorial Guinea',
				'ER' => 'Eritrea',
				'EE' => 'Estonia',
				'ET' => 'Ethiopia',
				'FK' => 'Falkland Islands (Malvinas)',
				'FO' => 'Faroe Islands',
				'FJ' => 'Fiji',
				'FI' => 'Finland',
				'FR' => 'France',
				'FX' => 'France, Metropolitan',
				'GF' => 'French Guiana',
				'PF' => 'French Polynesia',
				'TF' => 'French Southern Territories',
				'GA' => 'Gabon',
				'GM' => 'Gambia',
				'GE' => 'Georgia',
				'DE' => 'Germany',
				'GH' => 'Ghana',
				'GI' => 'Gibraltar',
				'GR' => 'Greece',
				'GL' => 'Greenland',
				'GD' => 'Grenada',
				'GP' => 'Guadeloupe',
				'GU' => 'Guam',
				'GT' => 'Guatemala',
				'GN' => 'Guinea',
				'GW' => 'Guinea-Bissau',
				'GY' => 'Guyana',
				'HT' => 'Haiti',
				'HM' => 'Heard and Mc Donald Islands',
				'VA' => 'Holy See (Vatican City State)',
				'HN' => 'Honduras',
				'HK' => 'Hong Kong',
				'HU' => 'Hungary',
				'IS' => 'Iceland',
				'IN' => 'India',
				'ID' => 'Indonesia',
				'IR' => 'Iran (Islamic Republic of)',
				'IQ' => 'Iraq',
				'IE' => 'Ireland',
				'IL' => 'Israel',
				'IT' => 'Italy',
				'JM' => 'Jamaica',
				'JP' => 'Japan',
				'JO' => 'Jordan',
				'KZ' => 'Kazakhstan',
				'KE' => 'Kenya',
				'KI' => 'Kiribati',
				'KP' => 'Korea, Democratic People\'s Republic of',
				'KR' => 'Korea, Republic of',
				'KW' => 'Kuwait',
				'KG' => 'Kyrgyzstan',
				'LA' => 'Lao People\'s Democratic Republic',
				'LV' => 'Latvia',
				'LB' => 'Lebanon',
				'LS' => 'Lesotho',
				'LR' => 'Liberia',
				'LY' => 'Libyan Arab Jamahiriya',
				'LI' => 'Liechtenstein',
				'LT' => 'Lithuania',
				'LU' => 'Luxembourg',
				'MO' => 'Macau',
				'MK' => 'Macedonia, Former Yugoslav Republic of',
				'MG' => 'Madagascar',
				'MW' => 'Malawi',
				'MY' => 'Malaysia',
				'MV' => 'Maldives',
				'ML' => 'Mali',
				'MT' => 'Malta',
				'MH' => 'Marshall Islands',
				'MQ' => 'Martinique',
				'MR' => 'Mauritania',
				'MU' => 'Mauritius',
				'YT' => 'Mayotte',
				'MX' => 'Mexico',
				'FM' => 'Micronesia, Federated States of',
				'MD' => 'Moldova, Republic of',
				'MC' => 'Monaco',
				'MN' => 'Mongolia',
				'MS' => 'Montserrat',
				'MA' => 'Morocco',
				'MZ' => 'Mozambique',
				'MM' => 'Myanmar',
				'NA' => 'Namibia',
				'NR' => 'Nauru',
				'NP' => 'Nepal',
				'NL' => 'Netherlands',
				'AN' => 'Netherlands Antilles',
				'NC' => 'New Caledonia',
				'NZ' => 'New Zealand',
				'NI' => 'Nicaragua',
				'NE' => 'Niger',
				'NG' => 'Nigeria',
				'NU' => 'Niue',
				'NF' => 'Norfolk Island',
				'MP' => 'Northern Mariana Islands',
				'NO' => 'Norway',
				'OM' => 'Oman',
				'PK' => 'Pakistan',
				'PW' => 'Palau',
				'PA' => 'Panama',
				'PG' => 'Papua New Guinea',
				'PY' => 'Paraguay',
				'PE' => 'Peru',
				'PH' => 'Philippines',
				'PN' => 'Pitcairn',
				'PL' => 'Poland',
				'PT' => 'Portugal',
				'PR' => 'Puerto Rico',
				'QA' => 'Qatar',
				'RE' => 'Reunion',
				'RO' => 'Romania',
				'RU' => 'Russian Federation',
				'RW' => 'Rwanda',
				'KN' => 'Saint Kitts and Nevis',
				'LC' => 'Saint Lucia',
				'VC' => 'Saint Vincent and The Grenadines',
				'WS' => 'Samoa',
				'SM' => 'San Marino',
				'ST' => 'Sao Tome And Principe',
				'SA' => 'Saudi Arabia',
				'SN' => 'Senegal',
				'SC' => 'Seychelles',
				'SL' => 'Sierra Leone',
				'SG' => 'Singapore',
				'SK' => 'Slovakia (Slovak Republic)',
				'SI' => 'Slovenia',
				'SB' => 'Solomon Islands',
				'SO' => 'Somalia',
				'ZA' => 'South Africa',
				'GS' => 'South Georgia, South Sandwich Islands',
				'ES' => 'Spain',
				'LK' => 'Sri Lanka',
				'SH' => 'St. Helena',
				'PM' => 'St. Pierre and Miquelon',
				'SD' => 'Sudan',
				'SR' => 'Suriname',
				'SJ' => 'Svalbard and Jan Mayen Islands',
				'SZ' => 'Swaziland',
				'SE' => 'Sweden',
				'CH' => 'Switzerland',
				'SY' => 'Syrian Arab Republic',
				'TW' => 'Taiwan',
				'TJ' => 'Tajikistan',
				'TZ' => 'Tanzania, United Republic of',
				'TH' => 'Thailand',
				'TG' => 'Togo',
				'TK' => 'Tokelau',
				'TO' => 'Tonga',
				'TT' => 'Trinidad and Tobago',
				'TN' => 'Tunisia',
				'TR' => 'Turkey',
				'TM' => 'Turkmenistan',
				'TC' => 'Turks and Caicos Islands',
				'TV' => 'Tuvalu',
				'UG' => 'Uganda',
				'UA' => 'Ukraine',
				'AE' => 'United Arab Emirates',
				'GB' => 'United Kingdom',
				'UM' => 'United States Minor Outlying Islands',
				'UY' => 'Uruguay',
				'UZ' => 'Uzbekistan',
				'VU' => 'Vanuatu',
				'VE' => 'Venezuela',
				'VN' => 'Vietnam',
				'VG' => 'Virgin Islands (British)',
				'VI' => 'Virgin Islands (U.S.)',
				'WF' => 'Wallis and Futuna Islands',
				'EH' => 'Western Sahara',
				'YE' => 'Yemen',
				'YU' => 'Yugoslavia',
				'ZM' => 'Zambia',
				'ZW' => 'Zimbabwe'
			);

			return apply_filters( 'ls-country-list', $country_list );
		
		} // End of get_country_options()

/**
 * Returns an array of language optins
 *
 * Region list from http://code.google.com/apis/maps/faq.html#languagesupport
 * Used for Maps v3 localization: http://code.google.com/apis/maps/documentation/javascript/basics.html#Localization
 * 
 * @access public
 *
 * @uses	apply_filters
 *
 * @return	array	$language_list	An array of language options
 */
		function get_language_options() {
		
			$language_list = array(
				'ar' => 'Arabic',
				'eu' => 'Basque',
				'bg' => 'Bulgarian',
				'bn' => 'Bengali',
				'ca' => 'Catalan',
				'cs' => 'Czech',
				'da' => 'Danish',
				'de' => 'German',
				'el' => 'Greek',
				'en' => 'English',
				'en-AU' => 'English (Australian)',
				'en-GB' => 'English (Great Britain)',
				'es' => 'Spanish',
				'eu' => 'Basque',
				'fa' => 'Farsi',
				'fi' => 'Finnish',
				'fil' => 'Filipino',
				'fr' => 'French',
				'gl' => 'Galician',
				'gu' => 'Gujarati',
				'hi' => 'Hindi',
				'hr' => 'Croatian',
				'hu' => 'Hungarian',
				'id' => 'Indonesian',
				'it' => 'Italian',
				'iw' => 'Hebrew',
				'ja' => 'Japanese',
				'kn' => 'Kannada',
				'ko' => 'Korean',
				'lt' => 'Lithuanian',
				'lv' => 'Latvian',
				'ml' => 'Malayalam',
				'mr' => 'Marathi',
				'nl' => 'Dutch',
				'no' => 'Norwegian',
				'pl' => 'Polish',
				'pt' => 'Portuguese',
				'pt-BR' => 'Portuguese (Brazil)',
				'pt-PT' => 'Portuguese (Portugal)',
				'ro' => 'Romanian',
				'ru' => 'Russian',
				'sk' => 'Slovak',
				'sl' => 'Slovenian',
				'sr' => 'Serbian',
				'sv' => 'Swedish',
				'tl' => 'Tagalog',
				'ta' => 'Tamil',
				'te' => 'Telugu',
				'th' => 'Thai',
				'tr' => 'Turkish',
				'uk' => 'Ukrainian',
				'vi' => 'Vietnamese',
				'zh-CN' => 'Chinese (Simplified)',
				'zh-TW' => 'Chinese (Traditional)',
			);

			return apply_filters( 'ls-language-list', $language_list );
		
		} // End of get_language_options()

/**
 * Returns an array of auto-locate options
 * 
 * @access public
 *
 * @uses	apply_filters
 *
 * @return	array	$auto_locate_list	An array of the auto-locate options
 */
		function get_auto_locate_options() {

			$auto_locate_list[''] 		= 'No Auto Location';
			$auto_locate_list['ip'] 	= 'IP Address';
			$auto_locate_list['html5'] 	= 'HTML5';
		
			return apply_filters( 'ls-auto-locte-list', $auto_locate_list );
		
		} // End of get_auto_locate_options()

/**
 * Echos the toolbar
 * 
 * @access 	public
 *
 * @global	$location_search
 *
 * @param 	string 	$title 	(default: '')
 *
 * @uses	get_options
 */
		function show_toolbar( $title = '' ) {
		
			global $location_search;
		
			$options = $location_search->get_options();
		
			if ( '' == $title )
				$title = 'LocationSearch';
			?>


			<?php
			/*
			if ( !isset( $options['api_key'] ) || $options['api_key'] == '' )
				echo '<div class="error"><p>' . __( 'You must enter an API key for your domain.', 'LocationSearch' ).' <a href="' . admin_url( 'admin.php?page=locationsearch' ) . '">' . __( 'Enter a key on the General Options page.', 'LocationSearch' ) . '</a></p></div>';
			*/

		} // End of show_toolbar()

/**
 * Returns the available search_radii
 * 
 * @access 	public
 *
 * @uses	apply_filters
 *
 * @return 	array	$search_radii	An array of the search radii
 */
		function get_search_radii(){
		
			$search_radii = array( 5, 10, 50, 100, 500, 1000, 10000 );

			return apply_filters( 'ls-search-radii', $search_radii );

		} // End of get_search_radii()

		//
/**
 *  What link are we using for google's API
 * 
 * @access public
 *
 * @uses	get_locale
 *
 * @return 	string	$api_link	
 */		
		function get_api_link() {
		
			$lo = str_replace('_', '-', get_locale());
			$l = substr($lo, 0, 2);
		
			switch($l) {
		
				case 'es':
				case 'de':
				case 'ja':
				case 'ko':
				case 'ru':
					$api_link = "http://code.google.com/intl/$l/apis/maps/signup.html";
					break;
				case 'pt':
				case 'zh':
					$api_link = "http://code.google.com/intl/$lo/apis/maps/signup.html";
					break;
				case 'en':
				default:
					$api_link = "http://code.google.com/apis/maps/signup.html";
					break;
		
			} // End of switch
		
			return $api_link;
		
		} // End of get_api_link()

/**
 * Returns true if legacy tables exist in the DB
 *
 * @access public
 *
 * @global	$wpdb
 *
 * @uses	get_results
 *
 * @return	bool	TRUE if the table exists, FALSE if not
 */
		function legacy_tables_exist() {
		
			global $wpdb;

			$sql = "SHOW TABLES LIKE '" . $wpdb->prefix . "location_search'";
		
			if ( $tables = $wpdb->get_results( $sql ) ) { return true; }

			return false;
		
		} // End of legacy_tables_exist()

/**
 * Search form / widget query vars
 * 
 * @access public
 *
 * @param 	array 	$vars
 *
 * @return 	array	$vars	An array of the query vars
 */
		function register_query_vars( $vars ) {

			$vars[] = 'location_search_address';
			$vars[] = 'location_search_city';
			$vars[]	= 'location_search_state';
			$vars[] = 'location_search_zip';
			$vars[] = 'location_search_distance';
			$vars[] = 'location_search_limit';
			$vars[] = 'location_is_search_results';

			return $vars;
		
		} // End of register_query_vars()

/**
 * Parses the shortcode attributes with the default options and returns array
 *
 * @since 	2.3
 * 
 * @param	array	$shortcode_atts		The attributes from the shortcode
 */
		function parse_shortcode_atts( $shortcode_atts ) {
		
			$options			= $this->get_options();
			$default_atts		= $this->get_default_shortcode_atts();
			$atts			 	= shortcode_atts( $default_atts, $shortcode_atts );

			// If deprecated shortcodes were used, replace with current ones
			if ( isset( $atts['show_categories_filter'] ) )
				$atts['show_ls_category_filter'] = $atts['show_categories_filter'];
			if ( isset( $atts['show_tags_filter'] ) )
				$atts['show_ls_tag_filter'] = $atts['show_tags_filter'];
			if ( isset( $atts['show_days_filter'] ) )
				$atts['show_ls_day_filter'] = $atts['show_days_filter'];
			if ( isset( $atts['show_times_filter'] ) )
				$atts['show_ls_time_filter'] = $atts['show_times_filter'];
			if ( isset( $atts['categories'] ) )
				$atts['ls_category'] = $atts['categories'];			
			if ( isset( $atts['tags'] ) )
				$atts['ls_tag'] = $atts['tags'];			
			if ( isset( $atts['days'] ) )
				$atts['ls_day'] = $atts['days'];			
			if ( isset( $atts['times'] ) )
				$atts['ls_time'] = $atts['times'];			

			// Determine if we need to hide the search form or not
			if ( '' == $atts['hide_search'] ) {

				// Use default value
				$atts['hide_search'] = ( 'show' == $options['display_search'] ? 0 : 1 );
						
			} 

			// Set categories and tags to available equivelants 
			$checks = array( 'ls_category', 'ls_tag', 'ls_day', 'ls_time' );

			foreach ( $checks as $check ) {

				$atts['avail_' . $check] 	= ( empty( $atts[$check] ) ? '' : $atts[$check] );

			} // End of $checks foreach loop

			// Default lat / lng from shortcode?
			if ( !$atts['default_lat'] ) 
				$atts['default_lat'] = $options['default_lat'];
			if ( !$atts['default_lng'] )
				$atts['default_lng'] = $options['default_lng'];

			// Doing powered by?
			$atts['powered_by'] = ( '' == $atts['powered_by'] ? $options['powered_by'] : ( 0 == $atts['powered_by'] ? 0 : 1 ) );

			// Default units or shortcode units?
			if ( 'km' != $atts['units'] && 'mi' != $atts['units'] )
				$atts['units'] = $options['units'];

			// Default radius or shortcode radius?
			$atts['radius'] = ( '' != $atts['radius'] && in_array( $atts['radius'], $this->get_search_radii() ) ? absint( $atts['radius'] ) : $options['default_radius'] );

			// Clean search_field_cols
			if ( 0 === absint( $atts['search_form_cols'] ) )
				$atts['search_form_cols'] = $default_atts['search_form_cols'];

			$checks = array( 'limit', 'map_type', 'map_height', 'map_width', 'zoom_level', 'autoload' );

			foreach ( $checks as $check ) {

				$key = ( $check == 'limit' ? 'results_limit' : $check );

				if ( '' == $atts[$check] ) { $atts[$check] = $options[$key]; }

			} // End of $checks foreach loop

			// Return final array
			return $atts;
		
		} // End of parse_shortcode_atts()
		
/**
 * Returns default shortcode attributes
 *
 * @since 2.3
 *
 * @uses	get_options
 * @uses	apply_filters
 *
 * @return	array	$atts	The filtered shortcode attributes
 */
		function get_default_shortcode_atts() {
			
			$options = $this->get_options();

			$tax_atts = array();
			$tax_search_fields = array();
			
			foreach ( $options['taxonomies'] as $taxonomy => $taxonomy_info ) {
			
				$tax_search_fields[] = "||labeltd_$taxonomy||empty";

				$safe_tax = str_replace('-', '_', $taxonomy);
				$tax_atts[$safe_tax] = '';
				$tax_atts['show_' . $safe_tax . '_filter'] = 1;

				// The following are deprecated. Don't use them.
				$tax_atts[strtolower($taxonomy_info['plural'])] = null;
				$tax_atts['show_' . strtolower($taxonomy_info['plural']) . '_filter'] = null;
			
			} // End of $options foreach loop

			$atts = $tax_atts + array(
				'search_title'				=> __( '', 'LocationSearch' ), 
				'search_form_type'			=> 'table', 
				'search_form_cols'			=> 3, 
				'search_fields'				=> 'labelbr_street||labelbr_city||labelbr_state||labelbr_zip||empty||empty||labeltd_distance||empty' . implode('', $tax_search_fields) . '||submit||empty||empty', 
				'taxonomy_field_type'		=> 'checkboxes',
				'hide_search'				=> '', 
				'hide_map'					=> 0, 
				'hide_list'					=> 0, 
				'default_lat'				=> 0, 
				'default_lng'				=> 0, 
				'adsense_publisher_id'		=> 0, 
				'adsense_channel_id'		=> 0, 
				'adsense_max_ads'			=> 0,
				'map_width'					=> '', 
				'map_height'				=> '', 
				'units'						=> '',
				'radius'					=> '',
				'limit'						=> '',
				'autoload'					=> '',
				'zoom_level'				=> '',
				'map_type'					=> '',
				'powered_by'				=> '', 
				'ls_day'					=> '',
				'ls_time'					=> ''
			);

			return apply_filters( 'ls-default-shortcode-atts', $atts );
		
		} // End of get_default_shortcode_atts()

/**
 * Filters category text labels
 * 
 * @access public
 *
 * @param mixed $text
 *
 * @return	string	The filtered text
 */
		function backwards_compat_categories_text( $text ) {
		
			return __( 'Categories', 'LocationSearch' );
		
		} // End of backwards_compat_categories_text()

/**
 * Filters category text labels
 * 
 * @access public
 *
 * @param mixed $text
 *
 * @return	string	The filtered text
 */
		function backwards_compat_tags_text( $text ) {
		
			return __( 'Tags', 'LocationSearch' );
		
		} // End of backwards_compat_tags_text()

/**
 * Filters category text labels
 * 
 * @access public
 *
 * @param mixed $text
 *
 * @return	string	The filtered text
 */
		function backwards_compat_days_text( $text ) {
		
			return __( 'Days', 'LocationSearch' );
		
		} // End of backwards_compat_days_text()

/**
 * Filters category text labels
 * 
 * @access public
 *
 * @param mixed $text
 *
 * @return	string	The filtered text
 */
		function backwards_compat_times_text( $text ) {
		
			return __( 'Times', 'LocationSearch' );
		
		} // End of backwards_compat_times_text()

/**
 * Create the shortcode
 *
 * @since	0.1
 *
 * @param	array	$atts		The attributes for the shortcode
 *
 * @uses	
 *
 * @return	mixed	$output		A clickable directory of lyrics
 */	
		function shortcode( $atts ) {

			ob_start();

			$this->mini_search();

			$output = ob_get_contents();
			
			ob_end_clean();
			
			return $output;

		} // End of shortcode()
		
/**
 * search_form function.
 *
 * @param 	string		$mode		The name for the mode hidden field
 *
 * @global  $ls_xml_search
 *
 * @uses	none_empty
 * @uses	zip_codes_first
 * @uses	get_post_custom
 * @uses	send_wp_mail
 * @uses	get_permalink
 * @uses	input_field
 * @uses	textarea
 * @uses	hidden_field
 * @uses	wp_nonce_field
 * @uses	esc_attr
 */
		function mini_search() {
			
			$action = ( is_home() ? home_url() : get_permalink() ); ?>
			
			<form method="post" action="<?php echo $action; ?>" id="careMiniSearch"><?php
			
			$input_args['class'] = $input_args['id'] = $input_args['name'] = 'ls_mini_zip';
			$input_args['placeholder'] 	= 'Zip / Postal Code';
			
			echo '<p class="mini_search_p">find home care ' . $this->make_text( $input_args );
			
			echo $this->make_hidden( array( 'name' => 'mode', 'value' => 'search' ) );

			// echo '<button class="mini_search_submit">&#xf138;</button>';

			// echo '<button class="mini_search_submit"><i class="icon-chevron-sign-right"></i></button>';
			
			$submit = esc_attr( '&gt;' );
			
			echo '<input type="submit" name="' . $submit .'" id="' . $submit .'" class="button ls_mini_submit" value="' . $submit . '" /></p>';
			
			echo '</form>';

			if ( isset( $_POST['mode'] ) && $_POST['mode'] == 'search' && !empty( $_POST['ls_mini_zip'] ) ) {

				global $ls_xml_search;

				$input['zip'] 					= sanitize_text_field( $_POST['ls_mini_zip'] );
				$loc_map['location_address'] 	= 'address';
				$loc_map['location_address2'] 	= 'address2';
				$loc_map['location_city'] 		= 'city';
				$loc_map['location_state'] 		= 'state';
				$loc_map['location_zip'] 		= 'zip';
				$loc_map['location_country'] 	= 'country';
				$loc_map['location_phone'] 		= 'phone';
				$loc_map['location_fax'] 		= 'fax';
				$loc_map['location_email'] 		= 'email';
				$loc_map['location_url'] 		= 'url';
				$loc_map['location_special'] 	= 'special';
				$found 							= $ls_xml_search->nnd_db_query( $input );
				
				if ( !$found || $found->post_count == 0 ) {

					echo '<p class="mini_search_error">No results, <a href="http://dev.nosleepforsheep.com/nnd-consumer/find-homecare/">search here</a> for nearby locations.</p>';
				
				} elseif ( $found->post_count == 1 ) {

					$permalink = get_permalink( $found->posts[0]->ID ); ?>
					<script type="text/javascript">
					<!--
					window.location = <?php echo '"' . $permalink . '"'; ?>
					//-->
					</script><?php
					
				} else {

					echo '<div id="minisearchresults">';

					foreach ( $found->posts as $post ) {

						echo '<p class="mini_search_result"><a href="' . get_permalink( $post->ID ) . '" class="mini_search_link">' . $post->post_title . '</a></p>';

					} // End of posts foreach loop

					echo '</div><!-- End of #results -->';

				} // End of $found check
				
			} elseif ( isset( $_POST['mode'] ) && $_POST['mode'] == 'search' && empty( $_POST['ls_mini_zip'] ) ) {
				
				echo '<p class="mini_search_error">Please enter a zip in the field.</p>';
				
			} // End of mode check
			
		} // End of mini_search()	



/* ==========================================================================
	Slushman Toolkit Functions
========================================================================== */

/**
 * Creates an hidden field based on the params
 *
 * @params are:
 *  name - (optional), can be a separate value from ID
 *	value - used for the value attribute
 *
 * @since	0.1
 * 
 * @param	array	$params		An array of the data for the hidden field
 *
 * @return	mixed	$output		A properly formatted HTML hidden field
 */			
		function make_hidden( $params ) { 
		
			extract( $params );
						
			$showname 	= ( !empty( $name ) ? ' name="' . $name . '"' : '' );
			$output 	= '<input type="hidden"' . $showname . 'value="' . ( !empty( $value ) ? $value : '' ) . '"' . ' />';
			
			return $output;
			
		} // End of make_hidden()

/**
 * Creates an input field based on the params
 *
 * Creates an input field based on the params
 * 
 * @params are:
 * 	class - used for the class attribute
 * 	desc - description used for the description span
 * 	id - used for the id and name attributes
 *	label - the label to use in front of the field
 *  name - (optional), can be a separate value from ID
 *  placeholder - The text that appears in th field before a value is entered.
 *  type - detemines the particular type of input field to be created, default is text
 *	value - used for the value attribute
 * 
 * Inputtype options: 
 *  email - email address
 *  file - file upload
 *  text - standard text field (default)
 *  tel - phone numbers
 *  url - urls
 *
 * @since	0.1
 * 
 * @param	array	$params		An array of the data for the text field
 *
 * @return	mixed	$output		A properly formatted HTML input field with optional label and description
 */			
		function make_text( $params ) { 
		
			extract( $params );
						
			$showid 	= ( !empty( $id ) ? ' id="' . $id . '" name="' . ( !empty( $name ) ? $name : $id ) . '"' : '' );
			$showclass 	= ( !empty( $class ) ? ' class="' . $class . '"' : '' );
			$showtype	= ' type="' . ( !empty( $type ) ? $type : 'text' ) . '"';
			$showvalue	= ( !empty( $value ) ? ' value="' . $value . '"' : 'value=""' );
			$showph		= ( !empty( $placeholder ) ? ' placeholder="' . $placeholder . '"' : '' );
			
			$output 	= ( !empty( $label ) ? '<label for="' . $id . '">' . $label . '</label>' : '' );
			$output 	.= '<input' . $showtype . $showid . $showvalue . $showclass . $showph . ' />';
			$output 	.= ( !empty( $desc ) ? '<br /><span class="description">' . $desc . '</span>' : '' );
			
			return $output;
			
		} // End of make_text()

/**
 * Check an array for any empty values
 *
 * Call the function like this:
 *
 * $this->none_empty( array( $item1, $item2, $item3, etc... ) )
 *
 * The output of this function is boolean, so you can use it in an if statement to check multiple values
 * instead of a long string of: !empty( $item1 ) && !empty( $item2 ) && etc..
 *
 * @param	array	$array		An array of data to check
 *
 * @return	bool	TRUE if all items are not empty, FALSE if any are empty
 */		
		function none_empty( $array ) {
		
			foreach ( $array as $item ) {
				
				if ( !empty( $item ) ) {
					
					continue;
					
				} else {
					
					return FALSE;
					
				}
				
			} // End of $array foreach
			
			return TRUE;
			
		} // End of none_empty()
		
/**
 * Display an array in a nice format
 *
 * @param	array	The array you wish to view
 */			
		function print_array( $array ) {

		  echo '<pre>';
		  
		  print_r( $array );
		  
		  echo '</pre>';
		
		} // End of print_array()
		
/**
 * Validates a phone number
 * 
 * @since	0.1
 *
 * @link	http://jrtashjian.com/2009/03/code-snippet-validate-a-phone-number/
 * 
 * @return	mixed	$phone | FALSE		Returns the valid phone number, FALSE if not
 */
		function sanitize_phone( $phone ) {
		
			if ( empty( $phone ) ) { return FALSE; }
			
			if( preg_match( '/^[+]?([0-9]?)[(|s|-|.]?([0-9]{3})[)|s|-|.]*([0-9]{3})[s|-|.]*([0-9]{4})$/', $phone ) ) {
			
				return trim( $phone );
			
			} // End of $phone validation
			
			return FALSE;
			
		} // End of sanitize_phone()

/**
 * Check if a shortcode is registered in WordPress.
 *
 * Examples: shortcode_exists( 'caption' ) - will return true.
 *           shortcode_exists( 'blah' )    - will return false.
 */
		function shortcode_exists( $shortcode = false ) {

			global $shortcode_tags;
		 
			if ( !$shortcode ) { return FALSE; }
		 
			if ( array_key_exists( $shortcode, $shortcode_tags ) ) { return TRUE; }
		 
			return false;

		} // End of shortcode_exists()				

/**
 * Creates an HTML textarea
 *
 * @params are:
 * 	class - used for the class attribute
 * 	desc - description used for the description span
 *  id - used for the id and name attributes
 *  name - (optional), can be a separate value from ID
 *	label - the label to use in front of the field
 *  placeholder - The text that appears in th field before a value is entered.
 *	value - used in the checked function
 *
 * @since	0.1
 * 
 * @param	array	$params		An array of the data for the textarea
 *
 * @return	mixed	$output		A properly formatted HTML textarea with optional label and description
 */
		function make_textarea( $params ) {
			
			extract( $params );
						
			$showclass 	= ( !empty( $class ) ? ' class="' . $class . '"' : '' );
			$showid 	= ( !empty( $id ) ? '" id="' . $id . '" name="' . ( !empty( $name ) ? $name : $id ) . '"' : '' );
			$showph		= ( !empty( $placeholder ) ? ' placeholder="' . $placeholder . '"' : '' );
			$showvalue	= ( !empty( $value ) ? esc_textarea( $value ) : '' );
			$style 		= 'cols="50" rows="10" wrap="hard"';
			
			$output 	= ( !empty( $label ) ? '<label for="' . $id . '">' . $label . '</label>' : '' );
			$output 	.= '<textarea ' . $showid . $showclass . $showph . $style . '>' . $showvalue . '</textarea>';
			$output 	.= ( !empty( $desc ) ? '<br /><span class="description">' . $desc . '</span>' : '' );
			
			return $output;
			
		} // End of make_textarea()



/*****************************		End of Functions		*****************************/		

	} // End of Location_Search
	
} // End of class check
?>