<?php
if ( !class_exists( 'LS_Locations' ) ) {

	class LS_Locations {
		
/**
 * Constructor
 *
 * @uses	add_action
 * @uses	add_filter
 */
		function __construct() {

			add_action( 'init', array( &$this, 'register_locations' ) );
			// add_action( 'init', array( &$this, 'register_location_taxonomies' ) );
			add_action( 'admin_enqueue_scripts', array( &$this, 'enqueue_location_add_edit_js' ) );
			add_action( 'init', array( &$this, 'location_add_edit_js' ) );
			add_action( 'save_post', array( &$this, 'save_post_meta' ) );
			add_action( 'wp_ajax_ajax_save_lat_lng', array( &$this, 'ajax_save_lat_lng' ) );
			add_filter( 'parse_request', array( &$this, 'limit_edit_query' ) );
			add_filter( 'quick_edit_dropdown_pages_args', array( &$this, 'limit_wp_dropdown_pages' ) );
			add_filter( 'wp_dropdown_pages', array( &$this, 'modify_empty_wp_dropdown_pages' ) );

			add_action( 'save_post', array( &$this, 'flush_cache_data' ) );
			add_action( 'trash_post', array( &$this, 'flush_cache_data' ) );
			add_action( 'untrash_post', array( &$this, 'flush_cache_data' ) );
			add_action( 'edit_post', array( &$this, 'flush_cache_data' ) );
			add_action( 'delete_post', array( &$this, 'flush_cache_data' ) );
			
		} // End of __construct()

/**
 * register_locations function.
 * 
 * @access	public
 * 
 * @global	$location_search
 *
 * @return	void
 */
		function register_locations() {

			global $location_search;

			$args 		= array();
			$options 	= $location_search->get_options();
			$plural 	= 'Locations';
			$single 	= 'Location';

			$args['publicly_queryable'] 			= TRUE;
			$args['exclude_from_search'] 			= FALSE;
			$args['rewrite']['with_front'] 			= FALSE;
			$args['rewrite']['slug'] 				= $options['permalink_slug'];
			$args['capability_type'] 				= 'post';
			$args['hierarchical'] 					= FALSE;
			$args['public'] 						= TRUE;
			$args['show_ui'] 						= TRUE;
			$args['query_var'] 						= TRUE;
			$args['menu_icon']						= plugins_url( '/images/icon.png', dirname( __FILE__ ) );
			$args['register_meta_box_cb'] 			= array( &$this, 'location_meta_cb' );
			$args['supports'] 						= array();
			$args['labels']['name'] 				= _x( $plural, 'post type general name' );
			$args['labels']['singular_name'] 		= _x( $single, 'post type singular name' );
			$args['labels']['all_items'] 			= __( 'Edit ' . $plural );
			$args['labels']['add_new'] 				= __( 'Add ' . $single );
			$args['labels']['add_new_item'] 		= __( 'Add ' . $single );
			$args['labels']['edit_item'] 			= __( 'Edit ' . $single );
			$args['labels']['new_item'] 			= __( 'New ' . $single );
			$args['labels']['view_item'] 			= __( 'View ' . $single );
			$args['labels']['search_items'] 		= __( 'Search ' . $plural );
			$args['labels']['not_found'] 			= __( 'No ' . $plural . ' Found' );
			$args['labels']['not_found_in_trash'] 	= __( 'No ' . $plural . ' Found in Trash' );
			$args['labels']['menu_name']			= __( $single . ' Search' );
			
			register_post_type( 'ls-location', $args );

		} // End of register_locations()

/**
 * register_location_taxonomies function.
 * 
 * @access	public
 *
 * @global	$location_search
 *
 * @return	void
 */
		function register_location_taxonomies() {

			global $location_search;

			$options = $location_search->get_options();

			foreach ( $options['taxonomies'] as $taxonomy => $tax_info ) {

				$this->register_location_taxonomy( $taxonomy, $tax_info );

			} // End of $options foreach loop

		} // End of register_location_taxonomies()

/**
 * register_location_taxonomy function.
 * 
 * @access	public
 *
 * @param	mixed	$taxonomy
 * @param	mixed	$tax_info
 *
 * @return	void
 */
		function register_location_taxonomy( $taxonomy, $tax_info ) {

			if ( taxonomy_exists( $taxonomy ) ) { return; }

			$tax_info += array(
				'singular' => $taxonomy,
				'plural' => $taxonomy,
				'hierarchical' => false,
			);

			$args['labels']['name'] 						= 'Location ' . $tax_info['plural'];
			$args['labels']['singular_name'] 				= 'Location ' . $tax_info['singular'];
			$args['labels']['search_items'] 				= 'Search ' . $tax_info['plural'];
			$args['labels']['popular_items'] 				= 'Popular ' . $tax_info['plural'];
			$args['labels']['all_items'] 					= 'All ' . $tax_info['plural'];
			$args['labels']['parent_item'] 					= 'Parent ' . $tax_info['singular'];
			$args['labels']['parent_item_colon'] 			= 'Parent ' . $tax_info['singular'] . ':';
			$args['labels']['edit_item'] 					= 'Edit ' . $tax_info['singular'];
			$args['labels']['update_item'] 					= 'Update ' . $tax_info['singular'];
			$args['labels']['add_new_item'] 				= 'Add New ' . $tax_info['singular'];
			$args['labels']['new_item_name'] 				= 'New ' . $tax_info['singular'] . ' Name';
			$args['labels']['separate_items_with_commas'] 	= 'Separate ' . strtolower( $tax_info['plural'] ) . ' with commas';
			$args['labels']['add_or_remove_items'] 			= 'Add or remove ' . strtolower( $tax_info['plural'] );
			$args['labels']['choose_from_most_used'] 		= 'Choose from the most used ' . strtolower( $tax_info['plural'] );
			$args['hierarchical'] 							= $tax_info['hierarchical'];
			$args['rewrite'] 								= TRUE;
			$args['show_tagcloud'] 							= FALSE;

			register_taxonomy( $taxonomy, 'ls-location', $args );

		} // End of register_location_taxonomy()

/**
 * location_meta_cb function.
 * 
 * @access	public
 *
 * @uses	remove_meta_box
 * @uses	add_meta_box
 *
 * @return	void
 */
		function location_meta_cb() {				    
		
			remove_meta_box( 'ls-categorydiv', 'ls-location', 'side' );
			remove_meta_box( 'tagsdiv-ls-tag', 'ls-location', 'side' );
					
			add_meta_box( 'ls-geo-location', __( 'Add Location', 'LocationSearch' ), array( &$this, 'geo_location' ), 'ls-location', 'normal' );
			add_meta_box( 'ls-content-blocks', __( 'Additional Content', 'LocationSearch' ), array( &$this, 'content_blocks' ), 'ls-location', 'normal' );
			add_meta_box( 'ls-social', __( 'Social Links', 'LocationSearch' ), array( &$this, 'social_links' ), 'ls-location', 'normal' );
			add_meta_box( 'ls-location-drag-drop', __( 'Location', 'LocationSearch' ), array( &$this, 'location_drag_drop' ), 'ls-location', 'side' );
			
		} // End of location_meta_cb()
		
/**
 * Creates the GeoLocation metabox
 * 
 * @access	public
 *
 * @global	$location_search
 * @global	$hook_suffix
 *
 * @param	mixed	$post
 *
 * @return	void
 */		
		function geo_location( $post ) {
		
			global $location_search, $hook_suffix;
		
			$options 			= $location_search->get_options();
			$custom  			= get_post_custom( $post->ID );
			$location_special 	= !empty( $custom['location_special'][0] ) ? $custom['location_special'][0] : ''; ?>

				<p class="sub"><?php _e('Please enter either an address or a latitude/longitude.', 'LocationSearch'); ?></p>

				<div class='hidden updated below-h2' id='js-geo-encode-msg'>
					<p><?php echo __( "Your Server's IP is over the geocode threshold set by Google so we had fallback to a Javascript function. <span id='ls_js_update_lat_lng_result'></span>", "LocationSearch" ); ?></p>
				</div>

				<div class="table">
					<table class="form-table">

						<!-- Store Address -->
						<tr valign="top">
							<td width="150"><label for="location_address"><?php _e('Address', 'LocationSearch'); ?></label></td>
							<td><input type="text" name="location_address" id="location_address" size="30" value="<?php echo esc_attr( !empty( $custom['location_address'][0] ) ? $custom['location_address'][0] : '' ); ?>" /><br />
							<input type="text" name="location_address2" size="30" value="<?php echo esc_attr( !empty( $custom['location_address2'][0] ) ? $custom['location_address2'][0] : '' ); ?>" /></td>
						</tr>
						
						<!-- City / Town -->
						<tr valign="top">
							<td><label for="location_city"><?php _e('City/Town', 'LocationSearch'); ?></label></td>
							<td><input type="text" name="location_city" id="location_city" value="<?php echo esc_attr( !empty( $custom['location_city'][0] ) ? $custom['location_city'][0] : '' ); ?>" size="30" /></td>
						</tr>

						<!-- Zip / Postal Code -->
						<tr valign="top">
							<td><label for="location_zip"><?php _e('Zip/Postal Code', 'LocationSearch'); ?></label></td>
							<td><?php
							
							// <input type="text" name="location_zip" id="location_zip" value="<?php echo esc_attr( !empty( $custom['location_zip'][0] ) ? $custom['location_zip'][0] : '' ); " size="100" maxlength="500" /></td>
							
							$textarea_args['class'] = $textarea_args['id'] = $textarea_args['name'] = 'location_zip';
							$textarea_args['value'] = ( !empty( $custom['location_zip'][0] ) ? $custom['location_zip'][0] : '' );
							
							echo $this->textarea( $textarea_args ); 
							
							?></td>
						</tr>

						<!-- Country -->
						<tr valign="top">
							<td><label for="location_country"><?php _e('Country', 'LocationSearch'); ?></label></td>
							<td>
								<select name="location_country" id="location_country"><?php

									foreach ( $location_search->get_country_options() as $key => $value ) {
									
										echo '<option value="' . $key . '" ' . selected( !empty( $custom['location_country'][0] ) ? $custom['location_country'][0] : $options['default_country'], $key, false ) . '>' . $value . '</option>'."\n";
									
									} // End of get_country_options foreach loop
								
								?></select>
							</td>
						</tr>
												
						<!-- State / Providence -->
						<tr valign="top">
							<td><label for="location_state"><?php _e('State/Province', 'LocationSearch'); ?></label></td>
							<td><input type="text" name="location_state" id="location_state" value="<?php echo esc_attr( !empty( $custom['location_state'][0] ) ? $custom['location_state'][0] : $options['default_state'] ); ?>" size="30" /></td>
						</tr>
						
						<!-- Lat / Lng -->						 
						<tr valign="top">
							<td><label for="location_lat"><?php _e('Latitude/Longitude', 'LocationSearch'); ?></label></td>
							<td><input type="text" name="location_lat" id="location_lat" size="14" value="<?php echo esc_attr( !empty( $custom['location_lat'][0] ) ? $custom['location_lat'][0] : '' ); ?>" />
							<input type="text" name="location_lng" id="location_lng" size="14" value="<?php echo esc_attr( !empty( $custom['location_lng'][0] ) ? $custom['location_lng'][0] : '' ); ?>" /> <span id='latlng_updated' class='updated' style='display:none;color:#666666;font-style:italic;font-size:11px;'>Lat / Lng updated. Update address too? <a href='#' onclick="dragDropUpdateAddress();jQuery('#latlng_updated').hide();jQuery('#latlng_dontforget').fadeIn();return false;" >yes</a> | <a href='#' onclick="jQuery('#latlng_updated').hide();jQuery('#latlng_dontforget').fadeIn();return false;">no</a></span> <span id='latlng_dontforget' class='error' style='display:none;color:#666666;font-style:italic;font-size:11px;'>Changes aren't saved until you update or publish</span></td>
						</tr>

						<!-- Phone -->
						<tr valign="top">
							<td width="150"><label for="location_phone"><?php _e('Phone', 'LocationSearch'); ?></label></td>
							<td><input type="text" id="location_phone" name="location_phone" size="30" maxlength="28" value="<?php echo esc_attr( !empty( $custom['location_phone'][0] ) ? $custom['location_phone'][0] : '' ); ?>" /></td>
						</tr>

						<!-- Email -->
						<tr valign="top">
							<td><label for="location_email"><?php _e('Email', 'LocationSearch'); ?></label></td>
							<td><input type="text" name="location_email" id="location_email" size="30" value="<?php echo esc_attr( !empty( $custom['location_email'][0] ) ? $custom['location_email'][0] : '' ); ?>" />
						</tr>

						<!-- Fax -->
						<tr valign="top">
							<td><label for="location_fax"><?php _e('Fax', 'LocationSearch'); ?></label></td>
							<td><input type="text" id="location_fax" name="location_fax" size="30" maxlength="28" value="<?php echo esc_attr( !empty( $custom['location_fax'][0] ) ? $custom['location_fax'][0] : '' ); ?>" /></td>
						</tr>

						<!-- Franchise Phone -->
						<tr valign="top">
							<td style="width:150px"><label for="ls_contact_number"><?php _e('Franchise Contact Number', 'LocationSearch'); ?></label></td>
							<td><input type="tel" name="ls_contact_number" id="ls_contact_number" size="30" value="<?php echo ( !empty( $custom['ls_contact_number'] ) ? esc_attr( $custom['ls_contact_number'][0] ) : '' ); ?>" />
						</tr>

						<!-- Caring Email -->
						<tr valign="top">
							<td style="width:150px"><label for="ls_caring_email"><?php _e('"Let Us Do The Caring" Email Address', 'LocationSearch'); ?></label></td>
							<td><input type="email" id="ls_caring_email" name="ls_caring_email" size="30" value="<?php echo ( !empty( $custom['ls_caring_email'] ) ? esc_attr( $custom['ls_caring_email'][0] ) : '' ); ?>" /></td>
						</tr>
<?php
/* SLUSHMAN: 513/2013 - add Career select menu to metabox. Don't forget to add get_career_pages_sels function */
?>
						<!-- Career Page URL -->
						<tr valign="top">
							<td style="width:150px"><label for="ls_career_page"><?php _e('Career Page', 'LocationSearch'); ?></label></td>
							<td><?php
							
							$args['blank'] 		= TRUE;
							$args['class']		= $args['id'] = $args['name'] = 'career_page_select';
							$args['value']		= ( !empty( $custom['career_page_select'] ) ? $custom['career_page_select'][0] : '' );
							$careers 			= $this->get_career_pages_sels();
							$args['selections'] = ( !$careers ? array() : $careers );

							echo $this->make_select( $args );

							?></td>
						</tr>

					</table>				
				</div> <!-- table -->
				<div class="clear"></div><?php
			
		} // End of geo_location()
		
/**
 * Creates the Content Blocks metabox
 * 
 * @access 	public
 *
 * @global	$location_search
 *
 * @param 	mixed 		$post
 *
 * @uses	get_options
 * @uses	get_post_custom
 * @uses	wp_editor
 * @uses	esc_attr
 *
 * @return 	void
 */
		function content_blocks( $post ) {
			
			global $location_search;
			
			$options = $location_search->get_options();
			$custom  = get_post_custom( $post->ID ); ?>
			
			<div class="table">
			
			<table class="form-table ls_content_blocks">
				 
				<!-- Right Content Block -->
				<tr>
					<td colspan="2" class="editor_row_label">Right Content Block</td>
				</tr>
				<tr valign="top">
					<td colspan="2"><?php
					
					echo '<div id="postdivrich" class="postarea ls_right_content_block">';
					
					$editor['dfw'] 					= TRUE;
					$editor['editor_height'] 		= 360;
					$editor['media_buttons'] 		= FALSE;
					$editor['tabfocus_elements'] 	= 'sample-permalink,post-preview';
					$editor['textarea_name'] 		= 'ls_right_content_block';
					
					$value = ( !empty( $custom['ls_right_content_block'][0] ) ? $custom['ls_right_content_block'][0] : '' );
		
					wp_editor( $value, 'ls_right_content_block', $editor );
						
					echo '</div>'; 
					
					?></td>
				</tr>

				<!-- Contact Info -->
				<tr>
					<td colspan="2" class="editor_row_label">Contact Info</td>
				</tr>
				<tr valign="top">
					<td colspan="2"><?php
					
					echo '<div id="postdivrich" class="postarea ls_contact_info">';
					
					$editor['dfw'] 					= TRUE;
					$editor['editor_height'] 		= 360;
					$editor['media_buttons'] 		= FALSE;
					$editor['tabfocus_elements'] 	= 'sample-permalink,post-preview';
					$editor['textarea_name'] 		= 'ls_contact_info';
					
					$value = ( !empty( $custom['ls_contact_info'][0] ) ? $custom['ls_contact_info'][0] : '' );
		
					wp_editor( $value, 'ls_contact_info', $editor );
						
					echo '</div>'; 
					
					?></td>
				</tr>
				
				<!-- Quotes -->
				<tr>
					<td colspan="2" class="editor_row_label">Quotes</td>
				</tr>
				<tr valign="top">
					<td colspan="2"><?php
					
					echo '<div id="postdivrich" class="postarea ls_quotes">';
					
					$editor['dfw'] 					= TRUE;
					$editor['editor_height'] 		= 360;
					$editor['media_buttons'] 		= FALSE;
					$editor['tabfocus_elements'] 	= 'sample-permalink,post-preview';
					$editor['textarea_name'] 		= 'ls_quotes';
					
					$value = ( !empty( $custom['ls_quotes'][0] ) ? $custom['ls_quotes'][0] : '' );
		
					wp_editor( $value, 'ls_quotes', $editor );
						
					echo '</div>'; 
					
					?></td>
				</tr>
				
			</table>
			
			</div> <!-- table -->
			
			<div class="clear"></div><?php
			
		} // End of content_blocks
		
/**
 * Creates the Social Links metabox
 * 
 * @access	public
 *
 * @param 	mixed 	$post
 *
 * @uses	get_post_custom
 * @uses	esc_url
 *
 * @return 	void
 */
		function social_links( $post ) {
			
			$custom = get_post_custom( $post->ID ); ?>
			
			<div class="table">
			
			<table class="form-table ls_content_blocks"><?php
			
			$socials = array( 'twitter', 'facebook', 'pinterest', 'googleplus', 'other_social' );
			
			foreach ( $socials as $social ) {
			
				$key 	= 'ls_' . $social;
				$caps 	= ucwords( str_replace( '_', ' ', ( $social == 'googleplus' ? 'google+' : $social ) ) );
				
				echo '<!--' . $caps . '-->'; ?>
				<tr valign="top">
					<td style="width:150px"><label for="<?php echo $key; ?>"><?php _e( $caps, 'LocationSearch'); ?></label></td>
					<td><input type="text" name="<?php echo $key; ?>" id="<?php echo $key; ?>" size="30" value="<?php echo esc_url( ( !empty( $custom[$key][0] ) ? $custom[$key][0] : '' ) ); ?>" />
				</tr><?php
				
			} // End of $socials foreach loop ?>
			
			</table>
	
			</div> <!-- table -->
			
			<div class="clear"></div><?php
			
		} // End of social_links
		
/**
 * location_drag_drop function.
 * 
 * @access	public
 *
 * @param	mixed	$post
 *
 * @return	void
 */		
		function location_drag_drop( $post ) {
		
			global $location_search;
			
			$options 	= $location_search->get_options();
			$custom 	= get_post_custom( $post->ID );

			$location_address 	= !empty( $custom['location_address'][0] ) ? $custom['location_address'][0] : '';
			$location_address2 	= !empty( $custom['location_address2'][0] ) ? $custom['location_address2'][0] : '';
			$location_city 		= !empty( $custom['location_city'][0] ) ? $custom['location_city'][0] : '';
			$location_state 	= !empty( $custom['location_state'][0] ) ? $custom['location_state'][0] : $options['default_state'];
			$location_zip 		= !empty( $custom['location_zip'][0] ) ? $custom['location_zip'][0] : '';
			$location_country 	= !empty( $custom['location_country'][0] ) ? $custom['location_country'][0] : $options['default_country'];
			$location_lat 		= !empty( $custom['location_lat'][0] ) ? $custom['location_lat'][0] : '';
			$location_lng 		= !empty( $custom['location_lng'][0] ) ? $custom[ 'location_lng'][0] : '';
		
			?><div id="dragdrop_map_canvas" style="width:267px;height:200px;"></div><?php
						
		} // End of location_drag_drop()

/**
 * Enqueues JS for location edit screen
 * 
 * @access public
 *
 * @global	$current_screen
 * @global	$post
 *
 * @return void
 */
		function enqueue_location_add_edit_js() {
		
			global $current_screen, $post;

			if ( !is_admin() || 'ls-location' != $current_screen->id ) { return; }
				
			wp_enqueue_script( 'ls-drag-drop-location-js', site_url() . '/?ls-drag-drop-location-js=' . $post->ID, array( 'jquery' ) );
			
		} // End of enqueue_location_add_edit_js()
		
/**
 * location_add_edit_js function.
 * 
 * @access public
 * @return void
 */		
		function location_add_edit_js() {
		
			global $current_screen, $location_search;
			
			$options = $location_search->get_options();
			
			if ( !isset( $_GET['ls-drag-drop-location-js'] ) ) { return; }
			
			$postid 	= (int)	$_GET['ls-drag-drop-location-js'];
			$options 	= $location_search->get_options();
			$custom 	= get_post_custom( $postid );
			
			$drag_drop_lat 	= !empty( $custom['location_lat'][0] ) ? $custom['location_lat'][0] : '40.730885';
			$drag_drop_lng 	= !empty( $custom['location_lng'][0] ) ? $custom['location_lng'][0] : '-73.997383';
			$drag_drop_zoom = ( $drag_drop_lat == '40.730885' ) ? 2 : 17;
			
			header( "Content-type: application/x-javascript" );

			/*
			if ( '' == $options['api_key'] ) {
				die( "alert( '" . esc_js( __( "You will need to enter your API key in general options before your addresses will be coded properly.", 'LocationSearch' ) ) . "');" );
			}
			*/

			?>

			var map;
			var geocoder;
			var address;
			var marker;
			var place;

			function location_add_edit_js_init() {
				var latlng = new google.maps.LatLng( <?php echo esc_js( $drag_drop_lat ); ?>, <?php echo esc_js( $drag_drop_lng ); ?> );
				var myOptions = {
					zoom: <?php echo esc_js( $drag_drop_zoom ); ?>,
					center: latlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				map = new google.maps.Map( document.getElementById( "dragdrop_map_canvas" ), myOptions );
				
				var image = new google.maps.MarkerImage(
				  '<?php echo esc_js( apply_filters( 'ls-search-marker-image-url', LOCATIONSEARCH_URL . "/images/heartmarker.png" ) ); ?>',
				  new google.maps.Size(20,34),
				  new google.maps.Point(0,0),
				  new google.maps.Point(0,34)
				);
				
				var shadow = new google.maps.MarkerImage(
				  '<?php echo esc_js( apply_filters( 'ls-search-marker-image-url', LOCATIONSEARCH_URL . "/images/heartshadow.png" ) ); ?>',
				  new google.maps.Size(40,34),
				  new google.maps.Point(0,0),
				  new google.maps.Point(0,34)
				);
				
				var shape = {
				  coord: [16,3,17,4,17,5,17,6,17,7,17,8,17,9,17,10,17,11,16,12,16,13,17,14,16,15,15,16,14,17,14,18,14,19,11,20,11,21,17,22,17,23,16,24,14,25,13,26,12,27,9,27,9,26,9,25,9,24,9,23,9,22,9,21,9,20,9,19,9,18,9,17,7,16,6,15,5,14,4,13,3,12,2,11,2,10,2,9,2,8,2,7,2,6,2,5,2,4,3,3,16,3],
				  type: 'poly'
				};

				marker = new google.maps.Marker({
					draggable: true,
					raiseOnDrag: false,
					icon: image,
					shadow: shadow,
					shape: shape,
					map: map,
					position: latlng,
					animation: google.maps.Animation.DROP
				});

				google.maps.event.addListener( map, 'click', dragDropGetAddress );
				google.maps.event.addListener( marker, 'dragend', dragDropGetAddressTest );

				geocoder = new google.maps.Geocoder();

				<?php
				// If PHP Geocode failed, do the ajax update
				if ( $ls_js_update = get_post_meta( $postid, 'ls-needs-js-geocode', true ) ) { ?>
					jQuery( '#js-geo-encode-msg' ).removeClass( 'hidden' );

					// Do geocode
					var geo_address = '<?php echo esc_js( $custom['location_address'][0] . ' ' . $custom['location_city'][0] . ' ' . $custom['location_state'][0] . ' ' . $custom['location_zip'][0] . '  ' . $custom['location_zip'][0] . ' ' . $custom['location_zip'][0] . '' . $custom['location_country'][0] ); ?>';
					geocoder.geocode( { 'address': geo_address }, function( results, status ) {
						if ( status == google.maps.GeocoderStatus.OK ) {
							var latlng = results[0].geometry.location;
							jQuery( "#location_lat" ).attr( 'value', latlng.lat() );
							jQuery( "#location_lng" ).attr( 'value', latlng.lng() );

							var lsdata = {
								action:			'ajax_save_lat_lng',
								ls_id:			<?php echo esc_js( $postid ); ?>,
								ls_lat:			latlng.lat(),
								ls_lng:			latlng.lng()
							};

							jQuery.post( ajaxurl, lsdata, function( response ) {
								jQuery( '#ls_js_update_lat_lng_result' ).html( response );
							});

							// Set drag/drop map to new location
							map.setCenter( latlng );
							map.setZoom( <?php echo esc_js( $drag_drop_zoom ); ?> );
							marker = new google.maps.Marker({
								map: map,
								position: latlng,
								draggable: true,
								animation: google.maps.Animation.DROP
							});

							jQuery( '#js-geo-encode-msg' ).removeClass( 'hidden' );
						}
						else {
							alert("Geocode was not successful for the following reason: " + status);
						}
					});
					<?php
					delete_post_meta( $postid, 'ls-needs-js-geocode' );
				}
				?>

			}

			function dragDropGetAddressTest() {
				var latlng = marker.getPosition();
				geocoder.geocode( { 'latLng': latlng }, dragDropShowAddress );
			}

			function dragDropGetAddress( event ) {
				if ( event.latlng != null ) {
					geocoder.geocode( { 'latLng': event.latlng }, dragDropShowAddress );
				}
			}

			function dragDropShowAddress( results, status ) {
				marker.setMap(null);
				if ( status != google.maps.GeocoderStatus.OK ) {
					alert("Geocoder failed due to: " + status);
					map.addOverlay(marker);
				} else {
					var latlng = results[0].geometry.location;
					marker = new google.maps.Marker({
						map: map,
						position: latlng,
						draggable: true,
						animation: google.maps.Animation.DROP
					});
					
					dragDropUpdateFormFields( latlng );
					place = results[0];

					google.maps.event.addListener( marker, 'dragend', dragDropGetAddressTest );
				}
			}
			
			jQuery(document).ready(function(){
				location_add_edit_js_init();
			});
			
			function dragDropUpdateFormFields( latlng ) {
				jQuery( "#location_lat" ).attr( 'value', latlng.lat() );
				jQuery( "#location_lng" ).attr( 'value', latlng.lng() );
				jQuery( "#latlng_dontforget" ).hide();
				jQuery( "#latlng_updated" ).fadeIn();
			}
			
			function dragDropUpdateAddress() {
				var newAddress = {};
				for ( var i = 0; i < place.address_components.length; i++ ) {
					var component = place.address_components[i];
					newAddress[component.types[0]] = component.short_name;
				}

				var newStreet = '';
				var newCity = '';
				var newState = '';
				var newZip = '';
				var newCountry = '';

				if ( newAddress.street_number ) {
					newStreet += newAddress.street_number;
				}
				if ( newAddress.route ) {
					if ( newStreet != '' ) {
						newStreet += ' ';
					}
					newStreet += newAddress.route;
				}

				if ( newAddress.locality ) {
					newCity += newAddress.locality;
				}

				if ( newAddress.administrative_area_level_1 ) {
					newState += newAddress.administrative_area_level_1;
				}

				if ( newAddress.postal_code ) {
					newZip += newAddress.postal_code;
				}

				if ( newAddress.country ) {
					newCountry += newAddress.country;
				}

				/*
				if ( place.AddressDetails.Country.AdministrativeArea != null && place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea != null && place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality != null && place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare != null )
				    newStreet = place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.Thoroughfare.ThoroughfareName;
				else if ( place.AddressDetails.Country.AdministrativeArea != null && place.AddressDetails.Country.AdministrativeArea.Locality != null && place.AddressDetails.Country.AdministrativeArea.Locality.Thoroughfare != null )
				    newStreet = place.AddressDetails.Country.AdministrativeArea.Locality.Thoroughfare.ThoroughfareName;

				if ( place.AddressDetails.Country.AdministrativeArea != null && place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea != null && place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality != null && place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName != null )
				    newCity = place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.LocalityName;
				else if ( place.AddressDetails.Country.AdministrativeArea != null && place.AddressDetails.Country.AdministrativeArea.Locality != null && place.AddressDetails.Country.AdministrativeArea.Locality.LocalityName != null )
				    newCity = place.AddressDetails.Country.AdministrativeArea.Locality.LocalityName;
				
				if ( place.AddressDetails.Country.AdministrativeArea != null && place.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName != null )
				    newState = place.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName;
				    
				if ( place.AddressDetails.Country.AdministrativeArea != null && place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea != null && place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality != null && place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.PostalCode != null && place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.PostalCode.PostalCodeNumber != null )
				    newZip = place.AddressDetails.Country.AdministrativeArea.SubAdministrativeArea.Locality.PostalCode.PostalCodeNumber;
				if ( place.AddressDetails.Country.AdministrativeArea != null && place.AddressDetails.Country.AdministrativeArea.Locality != null && place.AddressDetails.Country.AdministrativeArea.Locality.PostalCode != null && place.AddressDetails.Country.AdministrativeArea.Locality.PostalCode.PostalCodeNumber != null )
				    newZip = place.AddressDetails.Country.AdministrativeArea.Locality.PostalCode.PostalCodeNumber;
				
				if ( place.AddressDetails.Country != null && place.AddressDetails.Country.CountryNameCode != null )
				    newCountry = place.AddressDetails.Country.CountryNameCode;
				
				oldStreet = jQuery("#location_address").attr( 'value' );
				oldCity = jQuery("#location_address").attr( 'value' );
				oldState = jQuery("#location_address").attr( 'value' );
				oldZip = jQuery("#location_address").attr( 'value' );
				oldCountry = jQuery("#location_address").attr( 'value' );
				*/

				jQuery("#location_address").attr( 'value', newStreet );
				jQuery("#location_city").attr( 'value', newCity );
				jQuery("#location_state").attr( 'value', newState );
				jQuery("#location_zip").attr( 'value', newZip );
				jQuery("#location_country").val( newCountry );
			}
			<?php
			die();
			
		} // End of location_add_edit_js()
		
/**
 * Saves meta box data from CPT
 *
 * @since	0.1
 *
 * @global	$location_search
 * @global	$current_screen
 *
 * @param	int		$post_id	The ID of the post
 *
 * @uses	current_user_can
 * @Uses	get_options
 * @uses	get_post_custom
 * @uses	sanitize_email
 * @uses	esc_textarea
 * @uses	sanitize_text_field
 * @uses	esc_attr
 * @uses	update_post_meta
 * @uses	delete_post_meta
 * @uses	add_post_meta
 * @uses	set_geocode_meta
 */		
		function save_post_meta( $postID ) {
		
			global $location_search, $current_screen;

			if ( !is_object( $current_screen ) || 'ls-location' != $current_screen->id || 'ls-location' != $current_screen->post_type ) { return $postID; }		
			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return $postID; }
			if ( !current_user_can( 'edit_post', $postID ) ) { return $postID; }
			if ( !current_user_can( 'edit_page', $postID ) ) { return $postID; }	
			
			$options 	= $location_search->get_options();
			$custom 	= get_post_custom( $postID );
			$checks 	= array( 'location_address', 'location_address2', 'location_city', 'location_state', 'location_zip', 'location_country', 'location_lat', 'location_lng', 'location_phone', 'location_fax', 'location_url', 'location_email', 'location_special', 'ls_right_content_block', 'ls_contact_info', 'ls_quotes', 'ls_contact_number', 'ls_caring_email', 'ls_twitter', 'ls_facebook' ,'ls_pinterest' ,'ls_googleplus', 'ls_other_social', 'career_page_select' );
			$oldmeta = $newmeta = array();
			
			foreach ( $checks as $check ) {

				if ( $check == 'location_state' ) {

					$posted 			= isset( $_POST[$check] ) ? trim( $_POST[$check] ) : $options['default_state'];
					$oldmeta[$check] 	= ( !empty( $custom[$check][0] ) ? $custom[$check][0] : $options['default_state'] );

				} elseif ( $check == 'location_country' ) {

					$posted 			= isset( $_POST[$check] ) ? trim( $_POST[$check] ) : $options['default_country'];
					$oldmeta[$check] 	= ( !empty( $custom[$check][0] ) ? $custom[$check][0] : $options['default_country'] );

				} else {

					$posted = ( isset( $_POST[$check] ) ? trim( $_POST[$check] ) : ' ' );
					$oldmeta[$check] = ( !empty( $custom[$check][0] ) ? $custom[$check][0] : '' );

				} // End of $check check
			
				switch ( $check ) {
	 			
	 				case 'ls_twitter'				:
	 				case 'ls_facebook' 				:
	 				case 'ls_pinterest'				:
	 				case 'ls_googleplus'			:
	 				case 'career_page_select' 		: 
	 				case 'ls_other'					: $newmeta[$check] = esc_url_raw( $posted ); break;
		 			case 'location_email' 			: 
		 			case 'ls_caring_email' 			: $newmeta[$check] = sanitize_email( $posted ); break;
		 			case 'ls_right_content_block' 	:
		 			case 'ls_quotes'				:
	 				case 'ls_contact_info' 			: $newmeta[$check] = $posted; break;
		 			case 'location_zip' 			: $newmeta[$check] = esc_textarea( $posted ); break;
		 			default							: $newmeta[$check] = sanitize_text_field( $posted );
		 			
	 			} // End of $check switch
				
				if ( $newmeta[$check] && $newmeta[$check] != $oldmeta[$check] ) {
				
					// If the new meta value does not match the old value, update it.
					update_post_meta( $postID, $check, $newmeta[$check] );
					
				} elseif ( $newmeta[$check] == '' && $oldmeta[$check] ) {
	
					// If there is no new meta value but an old value exists, delete it.
					delete_post_meta( $postID, $check, $oldmeta[$check] );
	
				} elseif ( $newmeta[$check] && $oldmeta[$check] == '' ) {
				
					// If a new meta value was added and there was no previous value, add it.
					add_post_meta( $postID, $check, $newmeta[$check], true );
				
				} // End of meta value checks
				
			} // End of $checks foreach loop
			
			$this->set_geocode_meta( $postID, $oldmeta );
			
		} // End of save_post_meta()
		
/**
 * Saves geocode meta data
 *
 * @since	0.1
 *
 * @param	int		$postID		The ID of the post
 * @param	array	$oldmeta	The old meta data from before the post was updated
 *
 * @uses	get_post_custom
 * @Uses	geocode_location
 * @uses	update_post_meta
 */		
		function set_geocode_meta( $postID, $oldmeta ) {
		
			global $location_search;
		
			$custom = get_post_custom( $postID );
			$checks = array( 'location_address', 'location_city', 'location_state', 'location_zip', 'location_country', 'location_lat', 'location_lng' );
			
			foreach ( $checks as $check ) {
			
				$new[$check] = ( !empty( $custom[$check][0] ) ? $custom[$check][0] : '' );
			
			} // End of $checks foreach loop

			if ( $oldmeta['location_address'] != $new['location_address'] || $oldmeta['location_city'] != $new['location_city'] || $oldmeta['location_state'] != $new['location_state'] || $oldmeta['location_zip'] != $new['location_zip'] || $oldmeta['location_country'] != $new['location_country'] || '' == $new['location_lat'] || '' == $new['location_lng'] ) {
				
				$geocode_result = $location_search->geocode_location( $new['location_address'], $new['location_city'], $new['location_state'], $new['location_zip'], $new['location_country'], '' );

				if ( $geocode_result && isset( $geocode_result['status'] ) && $geocode_result['status'] == 'OK' ) {
					
					if ( isset( $geocode_result['lat'] ) && isset( $geocode_result['lng'] ) ) {
					
						update_post_meta( $postID, 'location_lat', $geocode_result['lat'] );
						update_post_meta( $postID, 'location_lng', $geocode_result['lng'] );
					
					}
					
				} else if ( $geocode_result && isset( $geocode_result['status'] ) ) {
					
					// Parse response
					switch( $geocode_result['status'] ) {
					
						case 620 :
						case 'OVER_QUERY_LIMIT' :
							update_post_meta( $postID, 'ls-needs-js-geocode', 'true' );
					
					} // End of switch
					
				} // End of $geocode_result check	
					
			} // End of meta_data checks
		
		} // End of set_geocode_meta()		
		
/**
 * limit_edit_query function.
 * 
 * @access	public
 *
 * @global	$current_screen
 * @global	$wpdb
 *
 * @param	mixed	$query
 *
 * @uses	get_results
 * @uses	add_action
 *
 * @return	mixed	$query
 */		
		function limit_edit_query( $query ) {
		
			global $current_screen, $wpdb;
			
			if ( is_object( $current_screen ) && 'edit-ls-location' == $current_screen->id ) {
				
				$sql = 'SELECT ID FROM `' . $wpdb->posts . '` WHERE post_type = "ls-location" AND post_status = "publish" LIMIT 10000';
				if ( 10000 == count( $wpdb->get_results( $sql ) ) ) {
					$query->query_vars['posts_per_page'] = $query->query_vars['posts_per_archive_page'] = 1000;
					add_action( 'in_admin_footer', array( &$this, 'print_excessive_locations_message' ) );
				}
				
			}
				
			return $query;
			
		} // End of limit_edit_query()
		
/**
 * print_excessive_locations_message function.
 * 
 * @access	public
 * @return	void
 */		
		function print_excessive_locations_message() {
		
			?>
			<div id="message" class="error"><p><?php _e( '<strong>Warning</strong>: You have more than 10,000 locations in your database. We have limited the list here to 1,000. You may <strong>use the search field to access locations beyond the first 1,000</strong>.', 'LocationSearch' ); ?></p></div>
			<?php
			
		} // End of print_excessive_locations_message()

/**
 * ajax_save_lat_lng function.
 * 
 * @access	public
 * @return	void
 */
		function ajax_save_lat_lng() {

			if ( empty( $_POST['ls_lat'] ) || empty( $_POST['ls_lng'] ) || empty( $_POST['ls_id'] ) )
				die( __( "It doesn't look like that worked either. Please try again later.", "LocationSearch" ) );
			
			$orig_lat = get_post_meta( absint( $_POST['ls_id'] ), 'location_lat', esc_attr( $_POST['ls_lat'], true ) );
			
			if ( !update_post_meta( absint( $_POST['ls_id'] ), 'location_lat', esc_attr( $_POST['ls_lat'] ) ) )
				die( __( "It doesn't look like that worked either. Please try again later.", "LocationSearch" ) );
			if ( !update_post_meta( absint( $_POST['ls_id'] ), 'location_lng', esc_attr( $_POST['ls_lng'] ) ) ) {
				update_post_meta( absint( $_POST['ls_id'] ), 'location_lat', esc_attr( $orig_lat ) );
				die( __( "It doesn't look like that worked either. Please try again later.", "LocationSearch" ) );
			}

			die( __( "It looks like that worked!", "LocationSearch" ) );
		
		} // End of ajax_save_lat_lng()

/**
 * limit_wp_dropdown_pages function.
 * 
 * @access	public
 *
 * @global	$current_screen
 *
  * @param	mixed	$args
 *
 * @return $args
 */
		function limit_wp_dropdown_pages( $args ) {
		
			global $current_screen;

			if ( is_object( $current_screen ) && 'edit-ls-location' == $current_screen->id ) {
			
				$args['include'] = 'This always returns an empty record set.';
			
			} // End of object check
				
			return $args;
			
		} // End of limit_wp_dropdown_pages()

/**
 * modify_empty_wp_dropdown_pages function.
 * 
 * @access	public
 *
 * @global $current_screen
 *
  * @param	mixed	$output
 *
 * @return	void
 */
		function modify_empty_wp_dropdown_pages( $output ) {
		
			global $current_screen;

			if ( is_object( $current_screen ) && 'edit-ls-location' == $current_screen->id ) {
			
				$output = '<select ide="post_parent" name="post_parent><option value="0>' . __( 'Main Page (no parent)' ) . '</option></select>';
				
			} // End object check
				
			return $output;
			
		} // End of modify_empty_wp_dropdown_pages()


		
/**
 * flush_cache_data function.
 * 
 * @access 	public
 *
 * @param 	int 	$id 	(default: 0)
 *
 * @uses	get_post_type
 * @uses	delete_transient
 *
 * @return 	void
 */
		function flush_cache_data( $id=0 ) {

			if ( 'ls-location' == get_post_type( $id ) || 'force' == $id ) {
			
				delete_transient( 'locationsearch-queries-cache' );
				
			}
			
		} // End of flush_cache_data()

/**
 * Returns an array of the career CPT post in for use in the make_select function
 * 
 * @uses	WP_Query
 *
 * @return 	bool | array 	$sels	FALSE if none are found, or an array of career post info
 */
		function get_career_pages_sels() {

			$args['post_type'] 					= 'slushman_cpt_careers';
			$args['posts_per_page'] 			= '-1';
			$args['post_status'] 				= 'publish';

			$careers = new WP_Query( $args );

			if ( $careers->post_count < 1 ) { return FALSE; }

			$i 		= 0;
			$sels 	= array();

			foreach ( $careers->posts as $career ) {

				$permalink	= get_permalink( $career->ID );
				$sels[$i] 	= array( 'label' => $career->post_title, 'value' => $permalink );
				$i++;

			} // End of $careers foreach loop

			return $sels;

		} // End of get_career_pages_sels()



/* ==========================================================================
   Slushman Toolkit Functions
   ========================================================================== */		

/**
 * Creates a select menu based on the params
 *
 * @params are:
 *  blank - false for none, true if you want a blank option, or enter text for the blank selector
 * 	class - used for the class attribute
 * 	desc - description used for the description span
 * 	id - used for the id and name attributes
 *	label - the label to use in front of the field
 *  name - the name of the field
 *	value - used in the selected function
 *	selections - an array of data to use as the selections in the menu
 *
 * @since	0.1
 * 
 * @param	array	$params		An array of the data for the select menu
 *
 * @return	mixed	$output		A properly formatted HTML select menu with optional label and description
 */	
		function make_select( $params ) {
			
			extract( $params );
			
			$showid 	= ( !empty( $id ) ? ' id="' . $id . '"' : '' );
			$showname 	= ' name="' . ( !empty( $name ) ? $name : ( !empty( $id ) ? $id : '' ) ) . '"';
			$showclass 	= ( !empty( $class ) ? ' class="' . $class . '"' : '' );
			
			$output = ( !empty( $label ) ? '<label for="' . $id . '">' . $label . '</label>' : '' );
			$output .= '<select' . $showid . $showname . $showclass .'>';
			$output .= ( !empty( $blank ) ? '<option>' . ( !is_bool( $blank ) ? __( $blank ) : '' ) . '</option>' : '' );
			
			if ( !empty( $selections ) ) {
			
				foreach ( $selections as $selection ) {
				
					extract( $selection, EXTR_PREFIX_ALL, 'sel' );
				
					$optvalue 	= ( !empty( $sel_value ) ? ' value="' . $sel_value . '"' : '' );
					$output 	.= '<option' . $optvalue . selected( $value, $sel_value, FALSE ) . ' >' . $sel_label . '</option>';
					
				} // End of $selections foreach
				
			} // End of $selections empty check
			
			$output .= '</select>';
			$output .= ( !empty( $desc ) ? '<br /><span class="description">' . $desc . '</span>' : '' );
			
			return $output;
						
		} // End of make_select()		

/**
 * Display an array in a nice format
 *
 * @param	array	The array you wish to view
 */			
		function print_array( $array ) {

		  echo '<pre>';
		  
		  print_r( $array );
		  
		  echo '</pre>';
		
		} // End of print_array()
				
/**
 * Creates an HTML textarea
 *
 * @params are:
 * 	class - used for the class attribute
 * 	desc - description used for the description span
 *  name - used for the name span
 *	label - the label to use in front of the field
 *	value - used in the checked function
 *
 * How to use:
 * 
 * $textarea_args['class'] 		= '';
 * $textarea_args['desc'] 		= '';
 * $textarea_args['id'] 		= '';
 * $textarea_args['name'] 		= '';
 * $textarea_args['label'] 		= '';
 * $textarea_args['value'] 		= '';
 * 
 * echo $this->textarea( $textarea_args );
 * 
 * 
 *
 * @since	0.1
 * 
 * @param	array	$params		An array of the data for the textarea
 *
 * @return	mixed	$output		A properly formatted HTML textarea with optional label and description
 */
		function textarea( $params ) {
			
			extract( $params );
						
			$showclass 	= ( !empty( $class ) ? ' class="' . $class . '"' : '' );
			
			$output 	= ( !empty( $label ) ? '<label for="' . $id . '">' . $label . '</label>' : '' );
			$output 	.= '<textarea name="' . $name . '" ' . $showclass . 'cols="50" rows="10" wrap="hard">' . esc_textarea( $value ) . '</textarea>';
			$output 	.= ( !empty( $desc ) ? '<br /><span class="description">' . $desc . '</span>' : '' );
			
			return $output;
			
		} // End of textarea()			

	} // End of LS_Locations

} // End of class check
?>