<?php
if ( !class_exists( 'LS_Options' ) ) {

	class LS_Options {

/**
 * __construct function.
 * 
 * @access public
 *
 * @uses	add_action
 *
 * @return void
 */
		function __construct() {
		
			add_action( 'admin_init', array( &$this, 'update_options' ) );
		
		} // End of __construct()

/**
 * Processes Options form if loaded
 * 
 * @access public
 *
 * @global	$location_search
 * @global	$ls_locations
 *
 * @uses	
 *
 * @return void
 */
		function update_options() {
		
			global $location_search, $ls_locations;

			// Delete all LocationSearch data.
			if ( isset( $_GET['ls-action'] )  && 'delete-locationsearch' == $_GET['ls-action'] ) {
			
				// Confirm we have both permisssion to do this and we have intent to do this.
				if ( current_user_can( 'manage_options' ) && ( check_admin_referer( 'delete-locationsearch-locations' ) || check_admin_referer( 'delete-locationsearch' ) ) ) {
			
					// Delete locations
					while ( $locations = query_posts( array( 'post_type' => 'ls-location', 'posts_per_page' => 200 ) ) ) {
			
						// Delete posts (and therby postmeta as well). Second arg bypasses trash
						foreach ( $locations as $key => $location ) {

							set_time_limit( 20 ); 
							wp_delete_post( $location->ID, true );

						}

					}

					$options = $location_search->get_options();
					$taxonomies = $options['taxonomies'];

					$original_taxonomies = array_keys( $location_search->get_taxonomy_settings() );

					if ( is_array( $original_taxonomies ) ) {

						foreach ( $original_taxonomies as $taxonomy ) {

							$taxonomies[$taxonomy] = true;

							if ( ! taxonomy_exists( $taxonomy ) ) {

								$ls_locations->register_location_taxonomy( $taxonomy, array() );

							}

						}

					}

					// Delete taxonomy terms
					$args = array( 'hide_empty' => 0 );

					if ( $terms = get_terms( array_keys( $taxonomies ), $args ) ) {

						foreach ( $terms as $key => $term ) {

							wp_delete_term( $term->term_id, $term->taxonomy );

						}

					}

					// Delete Options
					if ( get_option( 'LocationSearch_options' ) && empty( $_GET['locations-only'] ) ) {

						delete_option( 'LocationSearch_options' );

					}

					do_action( 'ls-delete-all-data' );

					wp_safe_redirect( admin_url( 'admin.php?page=locationsearch' ) );

				}

			}

			$options = $location_search->get_options();

			// Update Options if form was submitted or if WordPress options doesn't exist yet.
			if ( isset( $_POST['ls_general_options_submitted'] ) ) {

				check_admin_referer( 'ls-general-options' );

				$new_options = $options;

				// Validate POST Options
				//$new_options['api_key'] 				= ( ! empty( $_POST['api_key'] ) ) ? $_POST['api_key'] : '';
				$new_options['map_width']				= ( ! empty( $_POST['map_width'] ) ) ? $_POST['map_width'] : $options['map_width'];
				$new_options['map_height']				= ( ! empty( $_POST['map_height'] ) ) ? $_POST['map_height'] : $options['map_height'];
				$new_options['default_lat']				= ( ! empty( $_POST['default_lat'] ) ) ? $_POST['default_lat'] : $options['default_lat'] ;
				$new_options['default_lng']				= ( ! empty( $_POST['default_lng'] ) ) ? $_POST['default_lng'] : $options['default_lng'] ;
				$new_options['zoom_level']				= ( isset( $_POST['zoom_level'] ) ) ? absint( $_POST['zoom_level'] ) : $options['zoom_level'] ;
				$new_options['default_radius']			= ( ! empty( $_POST['default_radius'] ) ) ? absint( $_POST['default_radius'] ) : $options['default_radius'] ;
				$new_options['map_type']				= ( ! empty( $_POST['map_type'] ) ) ? $_POST['map_type'] : $options['map_type'];
				$new_options['special_text']			= ( isset( $_POST['special_text'] ) ) ? $_POST['special_text'] : $options['special_text'];
				$new_options['default_state']			= ( ! empty( $_POST['default_state'] ) ) ? $_POST['default_state'] : $options['default_state'];
				$new_options['default_country']			= ( ! empty( $_POST['default_country'] ) ) ? esc_attr( $_POST['default_country'] ) : $options['default_country'];
				$new_options['default_domain']			= ( ! empty( $_POST['default_domain'] ) ) ? $_POST['default_domain'] : $options['default_domain'];
				$new_options['address_format']			= ( ! empty( $_POST['address_format'] ) ) ? $_POST['address_format'] : $options['address_format'];
				$new_options['map_stylesheet']			= ( ! empty( $_POST['map_stylesheet'] ) ) ? $_POST['map_stylesheet'] : $options['map_stylesheet'];
				$new_options['units']					= ( ! empty( $_POST['units'] ) ) ? $_POST['units'] : $options['units'];
				$new_options['results_limit']			= ( isset( $_POST['results_limit'] ) ) ? absint( $_POST['results_limit'] ) : $options['results_limit'];
				$new_options['autoload']				= ( ! empty( $_POST['autoload'] ) ) ? $_POST['autoload'] : $options['autoload'];
				$new_options['map_pages']				= ( isset( $_POST['map_pages'] ) ) ? $_POST['map_pages'] : $options['map_pages'];
				$new_options['lock_default_location']	= ( ! empty( $_POST['lock_default_location'] ) ) ? true : $options['lock_default_location'];
				$new_options['powered_by']				= ( isset( $_POST['powered_by'] ) && 'on' == $_POST['powered_by'] ) ? 1 : 0;
				$new_options['enable_permalinks'] 		= ( isset( $_POST['enable_permalinks'] ) && 'on' == $_POST['enable_permalinks'] ) ? 1 : 0;
				$new_options['permalink_slug'] 			= ( ! empty( $_POST['permalink_slug'] ) ) ? $_POST['permalink_slug'] : $options['permalink_slug'];
				$new_options['adsense_for_maps'] 		= ( isset( $_POST['adsense_for_maps'] ) && 'on' == $_POST['adsense_for_maps'] ) ? 1 : 0;
				$new_options['adsense_pub_id'] 			= ( isset( $_POST['adsense_pub_id'] ) ) ? $_POST['adsense_pub_id'] : $options['adsense_pub_id'];
				$new_options['adsense_channel_id'] 		= ( isset( $_POST['adsense_channel_id'] ) ) ? $_POST['adsense_channel_id'] : $options['adsense_channel_id'];
				$new_options['adsense_max_ads'] 		= ( isset( $_POST['adsense_max_ads'] ) ) ? absint( $_POST['adsense_max_ads'] ) : $options['adsense_max_ads'];
				$new_options['display_search'] 			= ( ! empty( $_POST['display_search'] ) ) ? $_POST['display_search'] : $options['display_search'];
				$new_options['auto_locate']				= ( isset( $_POST['auto_locate'] ) ) ? $_POST['auto_locate'] : $options['auto_locate'];

				foreach ( $new_options['taxonomies'] as $taxonomy => $tax_info ) {
			
					if ( isset( $_POST['taxonomies'][$taxonomy]['active'] ) ) {
			
						$new_tax_options = $_POST['taxonomies'][$taxonomy];
						unset($new_tax_options['active']);
						//echo 'UPDATE(' . $taxonomy . ' - ' . json_encode( array_diff_assoc( array_filter( $new_tax_options ), $tax_info ) ) . ')' . PHP_EOL;
						$new_options['taxonomies'][$taxonomy] = array_filter( $new_tax_options ) + $tax_info;
						unset($_POST['taxonomies'][$taxonomy]);
			
					} else {
					
						//echo 'DISABLE(' . $taxonomy . ')' . PHP_EOL;
						unset($new_options['taxonomies'][$taxonomy]);
					
					}
				
				}

				if ( isset( $_POST['taxonomies'] ) ) {
				
					foreach ( $_POST['taxonomies'] as $taxonomy => $tax_info ) {
				
						if ( isset( $tax_info['active'] ) ) {
				
							//echo 'ENABLE(' . $taxonomy . ')' . PHP_EOL;
							$new_options['taxonomies'][$taxonomy] = $location_search->get_taxonomy_settings( $taxonomy );
				
						}
				
					}
				
				}

				$new_options = apply_filters( 'ls-new-general-options', $new_options, $options );

				if ( $new_options !== $options && update_option( 'LocationSearch_options', $new_options ) ) {
				
					if ( $new_options['enable_permalinks'] !== $options['enable_permalinks'] || $new_options['permalink_slug'] !== $options['permalink_slug'] ) {
                
                        update_option( 'ls-rewrite-rules', true );
				
					}

					do_action( 'ls-general-options-updated' );
					wp_redirect( admin_url( 'admin.php?page=locationsearch&ls-msg=1' ) );
					die();
				
				}
			
			}
			
		} // End of update_options()
		
/**
 * Prints the options page
 * 
 * @access public
 *
 * @global	$location_search
 * @global	$wpdb
 *
 * @return void
 */
		function print_page(){
		
			global $location_search, $wpdb;
		
			$options = $location_search->get_options();

			extract( $options );
			
			// Set Autoload Vars
			$count = $wpdb->get_col( "SELECT COUNT(ID) FROM `" . $wpdb->posts . "` WHERE post_type = 'ls-location' AND post_status = 'publish'" );

			if ( $count >= 250 ) {
		
				$disabled_autoload = false; // let it happen. we're limiting to 500 in the query
				$disabledmsg = sprintf( __( 'You have to many locations to auto-load them all. Only the closest %d will be displayed if auto-load all is selected.', 'LocationSearch' ), '250' );
		
			} else {
		
				$disabled_autoload = false;
				$disabledmsg = ''; 
		
			}
			
			// Extract styles
			$themes1 = $themes2 = array();

			if ( file_exists( LOCATIONSEARCH_PATH . '/inc/styles' ) )
				$themes1 = $this->read_styles( LOCATIONSEARCH_PATH . '/inc/styles' );
			
			if ( file_exists( WP_PLUGIN_DIR . '/locationsearch-styles' ) )
				$themes2 = $this->read_styles( WP_PLUGIN_DIR . '/locationsearch-styles' );
			
			$themes1 = apply_filters( 'ls-general-options-themes1', $themes1 );
			$themes2 = apply_filters( 'ls-general-options-themes1', $themes2 );
			?>
			<div class="wrap">
				
				<?php
				// Title
				$ls_page_title = apply_filters( 'ls-general-options-page-title', 'Options' );
				
				// Toolbar
				$location_search->show_toolbar( $ls_page_title );
					
				// Messages			 
				if ( isset( $_GET['ls-msg'] ) && '1' == $_GET['ls-msg'] )
					echo '<div class="updated fade"><p>'.__('Settings saved.', 'LocationSearch').'</p></div>';
				
				?>
				
				<div id="dashboard-widgets-wrap" class="clear">
				
				<form method="post" action="">
					<input type="hidden" name="ls_general_options_submitted" value="1" />
					
					<?php wp_nonce_field( 'ls-general-options' ); ?>
			
					<?php do_action( 'ls-general-options-page-top' ); ?>
			
					<div id='dashboard-widgets' class='metabox-holder'>
					
						<?php do_action( 'ls-general-options-dash-widgets-top' ); ?>

						<div class='postbox-container' style='width:49%;'>
						
							<div id='normal-sortables' class='meta-box-sortables ui-sortable'>
							
								<?php do_action( 'ls-general-options-normal-sortables-top' ); ?>
								
								<div class="postbox">
									
									<h3><?php _e( 'Options', 'LocationSearch' ); ?></h3>
									

									<div class="inside">
										
										<div class="table">
											<table class="form-table">
												<?php /*
												<tr valign="top">
													<td width="150"><label for="api_key"><?php _e( 'Google Maps API Key', 'LocationSearch' ); ?></label></td>
													<td>
														<input type="text" name="api_key" id="api_key" size="50" value="<?php echo esc_attr( $api_key ); ?>" /><br />
														<small><em><?php printf( __( '%s Click here%s to sign up for a Google Maps API key for your domain.', 'LocationSearch' ), '<a href="' . $location_search->get_api_link() . '">', '</a>'); ?></em></small>
													</td>
												</tr>
												*/ ?>
												
												<tr valign="top">
													
													<?php 
													if ( TRUE ) {
														$disabled_api = false;
														$api_how_to = __( 'Type in an address, state, or zip to locate the default location.', 'LocationSearch' );
													} else {
														$disabled_api = true;
														$api_how_to = __( 'After you enter an API Key, you can type in an address, state, or zip here to geocode the default location.', 'LocationSearch' );
													}
													?>
													
													<td width="150"><label for="default_lat"><?php _e( 'Starting Location', 'LocationSearch' ); ?></label></td>
													<td>
														<label for="default_lat" style="display: inline-block; width: 60px;"><?php _e( 'Latitude:', 'LocationSearch' ); ?> </label>
														<input type="text" name="default_lat" id="default_lat" size="13" value="<?php echo esc_attr( $default_lat ); ?>" /><br />
														<label for="default_lng" style="display: inline-block; width: 60px;"><?php _e( 'Longitude:', 'LocationSearch' ); ?> </label>
														<input type="text" name="default_lng" id="default_lng" size="13" value="<?php echo esc_attr( $default_lng ); ?>" />
													</td>
												</tr>
												
												<tr valign="top">
													<td><label for="default_radius"><?php _e( 'Search Radius', 'LocationSearch' ); ?></label></td>
													<td>
														<select name="default_radius" id="default_radius">
															<?php
															foreach ( $location_search->get_search_radii() as $value ) {
																echo "<option value='" . esc_attr( $value ) . "' " . selected( $value, $default_radius, false ) . ">" . esc_attr( $value ) . " " . esc_attr( $units ) . "</option>\n";
															}
															?>
														</select>
													</td>
												</tr>
												
												<tr valign="top">
													<td><label for="units"><?php _e( 'Distance Units', 'LocationSearch' ); ?></label></td>
													<td>
														<select name="units" id="units">
															<option value="mi" <?php selected( $units, 'mi' ); ?>><?php _e( 'Miles', 'LocationSearch' ); ?></option>
															<option value="km" <?php selected( $units, 'km' ); ?>><?php _e( 'Kilometers', 'LocationSearch' ); ?></option>
														</select>
													</td>
												</tr>
												
												<tr valign="top">
													<td><label for="results_limit"><?php _e( 'Results to Display', 'LocationSearch' ); ?></label></td>
													<td>
														<select name="results_limit" id="results_limit">
															<option value="0" <?php selected( $results_limit, 0 ); ?>>No Limit</option>
															<?php
															for ( $i = 5; $i <= 50; $i += 5 ) {
																echo "<option value='" . esc_attr( $i ) . "' " . selected( $results_limit, $i, false ) . ">" . esc_attr( $i ) . "</option>\n";
															}
															?>
														</select><br />
													</td>
												</tr>
																																		

											
											</table>
											
										</div> <!-- table -->
					
										
									</div> <!-- inside -->
										
									
									<div class="inside">
										
										<div class="table">
											<table class="form-table">
												
												<tr valign="top">
													<td width="150"><label for="map_width"><?php _e( 'Map Size', 'LocationSearch' ); ?></label></td>
													<td>
														<label for="map_width" style="display: inline-block; width: 60px;"><?php _e( 'Width:', 'LocationSearch' ); ?> </label>
														<input type="text" name="map_width" id="map_width" size="13" value="<?php echo esc_attr( $map_width ); ?>" /><br />
														<label for="map_height" style="display: inline-block; width: 60px;"><?php _e( 'Height:', 'LocationSearch' ); ?> </label>
														<input type="text" name="map_height" id="map_height" size="13" value="<?php echo esc_attr( $map_height ); ?>" /><br />
													</td>
												</tr>	


 												<tr valign="top">
													<td><label for="zoom_level"><?php _e('Zoom Level', 'LocationSearch'); ?></label></td>
													<td>
														<select name="zoom_level" id="zoom_level">
															<option value='0' <?php selected( $zoom_level, 0 ); ?> >Auto Zoom</option>
															<?php
															for ( $i = 1; $i <= 19; $i++ ) {
																echo "<option value='" . esc_attr( $i ) . "' " . selected( $zoom_level, $i, false ) . ">" . esc_attr( $i ) . "</option>\n";
															}
															?>
														</select><br />
													</td>
												</tr>
												
												
												<tr valign="top">
													<td><label for="auto_locate"><?php _e( 'Auto-detect Location', 'LocationSearch' ); ?></label></td>
													<td>
														<select name="auto_locate" id="auto_locate">
															<?php
															foreach ( $location_search->get_auto_locate_options() as $value => $label ) {
																echo "<option value='" . esc_attr( $value ) . "' " . selected( $value, $auto_locate, false ) . ">" . esc_attr( $label ) . "</option>\n";
															}
															?>
														</select><br />
													</td>
												</tr>
												
  																			
											</table>
										</div> <!-- table -->
															<p class="sub"><?php printf( __( 'Shortcode: %s', 'LocationSearch' ), '<code>[locationsearch]</code>' ); ?></p>
										<p class="submit" align="right">
											<input type="submit" class="button-primary" value="<?php _e( 'Save Options', 'LocationSearch' ) ?>" />&nbsp;&nbsp;
										</p>
										<div class="clear"></div>
										
									</div> <!-- inside -->
										
										
										
									</div> <!-- inside -->
								</div> <!-- postbox -->
								
								<?php do_action( 'ls-general-options-normal-sortables-bottom' ); ?>
								
								</div>
							</div>
								
							<div class='postbox-container' style='width:49%;'>
								
								<div id='side-sortables' class='meta-box-sortables ui-sortable'>
								
								<?php do_action( 'ls-general-options-side-sortables-top' ); ?>
								


								<?php do_action( 'ls-general-options-side-sortables-bottom' ); ?>
							
							</div> <!-- meta-box-sortables -->
						</div> <!-- postbox-container -->
					
						<?php do_action( 'ls-general-options-dash-widgets-bottom' ); ?>
					
					</div> <!-- dashboard-widgets -->
					</form>
										
					<div class="clear">
					</div>
				</div><!-- dashboard-widgets-wrap -->
			</div> <!-- wrap -->
			<?php
			
		} // End of print_page()
		
/**
 * Locate and list style options / location
 * 
 * @access 	public
 *
 * @param 	mixed 	$dir
 *
 * @uses	_cleanup_header_comment
 *
 * @return 	void
 */
		function read_styles( $dir ) {

			$themes = array();

			if ( $handle = opendir( $dir ) ) {

			    while ( false !== ( $file = readdir( $handle ) ) ) {

			        if ( $file != "." && $file != ".." && $file != ".svn" && $file != 'admin.css' ) {
			        	$theme_data = implode( '', file( $dir . '/' . $file ) );
			
						$name = '';
						$name = ( preg_match( '|Theme Name:(.*)$|mi', $theme_data, $matches ) ? _cleanup_header_comment( $matches[1] ) : basename( $file ) );

						$themes[$file] = $name;

			        }

			    } // End of while loop

			    closedir( $handle );

			}

			return( $themes );

		} // End of read_styles()

	} // End of LS_Options

} // End of class check
?>
